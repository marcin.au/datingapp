import React, { useState, useEffect } from 'react'
import { Image, StyleSheet } from 'react-native'
import { useSelector } from 'react-redux'
import CameraGreen from '../assets/icnos/camera-green.png'
import CameraYellow from '../assets/icnos/camera-yellow.png'
import ProgressCircle from 'react-native-progress-circle'
import colors from '../assets/colors'
import env from '../env'

const ReadyModeIcon = () => {
  const [progress, setProgress] = useState(0)
  const [timer, setTimer] = useState(null)
  const [showProgress, setShowProgress] = useState(false)
  const readyMode = useSelector(state => state.video.readyMode)
  const readyModeData = useSelector(state => state.video.readyModeData)

  useEffect(() => {
    if (readyModeData.wantsToChangeTo === null) return
    if (timer) clearInterval(timer)
    const currentTime = Math.floor(Date.now() / 1000)
    setProgress(currentTime - readyModeData.lastChange)
    setTimer(setInterval(() => {
      setProgress(progress => progress + 1)
    }, 1000)) 
    setShowProgress(true)
  }, [readyModeData.wantsToChangeTo])

  useEffect(() => {
    if (progress < env.readyModeTimeout) return
    clearInterval(timer)
    setTimer(null)
    setProgress(0)
    setShowProgress(false)
  }, [progress])

  const percentValue = progress / env.readyModeTimeout * 100

  const progressCircle = (
    <ProgressCircle
      percent={percentValue}
      radius={14}
      borderWidth={4}
      color={colors.button}
      shadowColor='#555'
      bgColor={colors.lightYellow}
    />
  )
  
  const icon = readyMode ? CameraGreen : CameraYellow
  const button = <Image source={icon} style={styles.icon} />
  return showProgress ? progressCircle : button
}

const styles = StyleSheet.create({
  icon: {
    width: 25,
    height: 25,
    resizeMode: 'contain'
  }
})

export default ReadyModeIcon