import React from 'react'
import { Text, StyleSheet } from 'react-native'

const TextWithFont = (props) => {
  return <Text style={{...styles.text ,...props.style }}>{props.children}</Text>
}

const styles = StyleSheet.create({
  text: {
    fontFamily: "Brown-Bold"
  },

})

export default TextWithFont