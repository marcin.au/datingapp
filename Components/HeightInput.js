import React, { useEffect } from 'react'
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'
import Picker from 'react-native-picker'
import PropTypes from 'prop-types'

const MIN_FEET = 3
const MAX_FEET = 8

const data = [
  ['3 ft', '4 ft', '5 ft', '6 ft', '7 ft', '8 ft'],
  ['0 in', '1 in', '2 in', '3 in', '4 in', '5 in', '6 in', '7 in', '8 in', '9 in', '10 in', '11 in']
]

const parseFrom = (str) => {
  const defaultValue = ['5 ft', '8 in']

  let [feet, inches] = str.split('\'')
  if (!feet || !inches || feet.length !== 1 || inches.length < 2) return defaultValue
  inches = inches.substring(0, inches.length - 1)
  feet = parseInt(feet)
  inches = parseInt(inches)
  if (isNaN(feet) || isNaN(inches)) return defaultValue
  if (feet < MIN_FEET || feet > MAX_FEET) return defaultValue
  if (inches < 0 || inches > 11) return defaultValue
  return [`${feet} ft`, `${inches} in`]
}

const parseTo = arr => {
  let [feet, inches] = arr
  feet = parseInt(feet.replace(' ft', ''))
  inches = parseInt(inches.replace(' in', ''))
  return `${feet}'${inches}"`
}

const HeightInput = (props) => {
  const { value, onChange } = props

  const pickerHandler = () => {
    Picker.init({
      pickerData: data,
      selectedValue: parseFrom(value),
      pickerBg: [220, 220, 220, 1],
      pickerTitleText: 'Your height',
      pickerCancelBtnText: 'Cancel',
      pickerConfirmBtnText: 'Confirm',
      onPickerConfirm: values => onChange(parseTo(values)),
    })
    Picker.show()
  }
 
  return (
    <TouchableOpacity style={styles.screen} onPress={() =>  pickerHandler()}>
      <Text style={{color: props.value == "" ? "grey" : "black", fontSize: 16, fontFamily: "Brown-Bold" }}>{props.value == "" ? "Height" : props.value}</Text>
    </TouchableOpacity>
  )
}

HeightInput.propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
}

const styles = StyleSheet.create({
  screen: {
    width: "100%",
    height: "100%",
    alignItems: 'center',
    justifyContent: "center"
  }
})

export default HeightInput
