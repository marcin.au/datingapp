import React,{useState} from 'react'
import { View, Image, StyleSheet, ActivityIndicator } from 'react-native'
import PropTypes from 'prop-types'

import test from '../assets/test.jpg'

const UserImage = (props) => {
  const [loading, setLoading] = useState(true)

  return (
    <View style={{ ...styles.container, ...props.style }}>
      <Image onLoadEnd={() => setLoading(false)} source={{ uri: props.src }} style={styles.image}/>
      <ActivityIndicator style={styles.spiner} size="large" color="black" animating={loading }/>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: 300,
    height: 300,
    shadowColor: 'black',
    shadowOpacity: 0.26,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 8,
    elevation: 6,
    borderRadius: 8,
  },
  image: {
    width: "100%",
    height: "100%",
    borderRadius: 8
  },
  spiner: {
    position : "absolute",
    margin: "auto",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0
  }
})

UserImage.propTypes = {
  src: PropTypes.string.isRequired,
  style: PropTypes.object,
}

UserImage.defaultProps = {
  style: {},
}

export default UserImage
