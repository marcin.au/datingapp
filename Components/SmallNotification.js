import React, { useEffect, useState, useRef } from 'react'
import {View, StyleSheet, Image, Platform, Dimensions } from 'react-native'
import Text from './Text'
import colors from '../assets/colors'
import { useSelector, useDispatch } from 'react-redux'
import { SET_SMALL_NOTIFICATION, SET_REJECT_NAME } from '../store/actions/video'
import Animated, { Easing } from 'react-native-reanimated'
import env from '../env'

const SmallNotification = (props) => {
  const [timer, setTimer] = useState(null)
  const notification = useSelector(state => state.video.smallNotification)
  const notificationData = useSelector(state => state.video.smallNotificationData)
  const rejectName = useSelector(state => state.video.rejectName)
  const dispatch = useDispatch()
  const slideAnimation = useRef(new Animated.Value(-dimensions.containerHeight)).current

  const slideDown = () => {
    Animated.timing(slideAnimation, {
      toValue: 0,
      duration: 750,
      easing: Easing.cubic
    }).start()
  }

  const slideUp = () => {
    Animated.timing(slideAnimation, {
      toValue: -dimensions.containerHeight,
      duration: 750,
      easing: Easing.cubic
    }).start()
  }

  useEffect(() => {
    if (notification) slideDown()
  }, [notification])

  useEffect(() => {
    if (!notification) return
    if (timer) clearTimeout(timer)
    setTimeout(() => {
      slideUp()
    }, 3500)
    setTimer(setTimeout(() => {
      dispatch({ type: SET_SMALL_NOTIFICATION, context: null })
      dispatch({ type: SET_REJECT_NAME, name: null })
    }, 4500))
  }, [notification])

  let notificationText = 'empty'
  if (notification === 'call-extended-by-you') notificationText = "You've requested for extend the time. Your partner have to do this also."
  if (notification === 'call-extended') notificationText = '5 Min added!'
  if (notification === 'shared-number') notificationText = 'You shared Your number'
  if (notification === 'rejected') notificationText = `${rejectName} can't talk right now, give us a sec to find one of your other matches for a date`
  if (notification === 'new-match') notificationText = `Congrats! You've matched with ${notificationData.name}`

  const url = () => {
    if (!notificationData.avatar) return
    return notificationData.avatar.replace('localhost', env.hostname)
  }

  const getContent = () => {
    if (notification !== 'new-match') {
      return <Text style={styles.text}>{notificationText}</Text>
    }
    return (
      <View style={styles.content}>
        <Image source={{ uri: url() }} style={styles.img}/>
        <View style={styles.contentText}>
          <Text style={{ fontSize: 18, textAlign: 'center' }}>{notificationText}</Text>
        </View>
      </View>
    )
  }

  return (
    <Animated.View style={{ ...styles.container, top: slideAnimation }}>
      <View style={{ ...styles.elipsis, ...styles.shadow }} />
      <View style={{ ...styles.rectangle, ...(Platform.OS === "android" ? styles.shadow : null ) }}>
        {getContent()}
      </View>
    </Animated.View>
  )
}

const dimensions = {
  notificationHeight: Platform.OS === 'ios' ? 200 : 150,
  additionalPaddingTop: Platform.OS === 'ios' ? 50 : 0,
  elipsisHeight: 40,
  containerHeight: (Platform.OS === 'ios' ? 200 : 150) + 20
}

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    width: "100%",
    alignItems: 'center'
  },
  rectangle: {
    width: '100%',
    flexDirection: 'row',
    backgroundColor: colors.lightYellow,
    height: dimensions.notificationHeight,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: dimensions.additionalPaddingTop,
    elevation: 4
  },
  elipsis: {
    position: 'absolute',
    top: dimensions.notificationHeight - dimensions.elipsisHeight / 2,
    width: '10%',
    backgroundColor: colors.lightYellow,
    height: dimensions.elipsisHeight,
    borderRadius: dimensions.elipsisHeight,
    transform: [{ scaleX: 10 }]
  },
  text: {
    width: "100%",
    fontSize: 18,
    textAlign: 'center',
    paddingHorizontal: 15,
    marginTop: 15
  },
  contentText: {
    justifyContent: 'center',
    alignItems: 'center',
    width: Dimensions.get('window').width - 170
  },
  shadow: {
    elevation: 4,
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
  },
  content: {
    flexDirection: 'row',
    width: "100%",
    textAlign: 'center',
    paddingHorizontal: 15,
    marginTop: 15,
  },
  img: {
    width: 120,
    height: 120,
    borderRadius: 60,
    margin: 10,
    marginTop: 10 + dimensions.elipsisHeight / 2,
  },
})

export default SmallNotification