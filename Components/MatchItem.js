import React from 'react'
import {View, StyleSheet, Image, TouchableOpacity} from 'react-native'
import Text from './Text'
import env from '../env'
import FontAwesome from 'react-native-vector-icons/FontAwesome'

import camera from '../assets/icnos/camera-green.png'

const MatchesItem = props => {

    let avatar = null
    if (props.avatar) {
        avatar = props.avatar.replace('localhost', env.hostname)
    }

    const defaultAvatar = (
        <FontAwesome name="user-circle" size={100} color="grey" style={{ marginRight: 20 }}/>
    )

    const userAvatar = (
        <Image style={styles.image}  source={{ uri: avatar }}/>
    )

    const jobPlacehoder = <Text style={{ color: 'grey' }}>Job</Text>
    const locationPlaceholder = <Text style={{ color: 'grey' }}>Location</Text>

    return (
        <TouchableOpacity style={styles.touchaleCmp} onPress={props.onSelect} onLongPress={props.drag} useForeground>
            <View style={styles.screen}>
                    {avatar ? userAvatar : defaultAvatar}
                    <View style={styles.infoBox}>
                        <Text style={styles.MatchName}>{props.name}, {props.age}</Text>
                        <Text style={styles.MatchInfo}>{props.job || jobPlacehoder}</Text>
                        <Text style={styles.MatchInfo}>{props.location ? `From: ${props.location}` : locationPlaceholder}</Text>
                    </View>
                <View style={styles.cameraBox}>{props.cam === true ? (
                        <TouchableOpacity onPress={props.startVideoCall}>
                        <Image source={camera} style={styles.camera}/>
                        </TouchableOpacity>)
                        : <View style={styles.camera}></View>}
                </View>
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    touchaleCmp:{
        width: "100%"
    },
    screen: {
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'space-around',
        height: 120,
        width: "100%",
        backgroundColor: "white",
        marginVertical: 2,
        paddingHorizontal: 20,
        zIndex: 1,
    },
    image: {
        width: 100,
        height: 100,
        borderRadius: 50,
        marginRight: 20
    },
    infoBox:{
        width: "60%",
        height: 90,
        justifyContent: 'space-around',
        marginHorizontal: 10
    },
    MatchName:{
        fontSize: 22,
        marginBottom: 8
    },
    MatchInfo: {
        fontSize: 14,
    },
    cameraBox:{
        zIndex: 0
    },
    camera: {
        height: 40,
        width: 40
    }
})

export default MatchesItem