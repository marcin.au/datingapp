import React, { useState, useEffect, useCallback } from 'react'
import { View, StyleSheet, Modal , Alert, NativeModules, Image, ActivityIndicator,TouchableOpacity, Dimensions } from 'react-native'
import {RtcEngine, AgoraView} from 'react-native-agora'
import * as Permissions from 'expo-permissions'
import ProgressCircle from 'react-native-progress-circle'
import { NavigationActions, StackActions } from 'react-navigation';
import Text from '../Components/Text'
import colors from '../assets/colors'
import Ad from '../Components/Ad'
import CameraView from '../Components/CameraView'
import Counter from '../Components/Counter'
import PhoneRedIcon from '../assets/icnos/sluchawka-czerwona.png'
import EndCallView from './EndCallView'
import Api from '../Api'
import { useSelector, useDispatch } from 'react-redux'
import { REMOVE_FROM_LIST, UPDATE_CONTACT } from '../store/actions/match'
import { UPDATE_CALL_ATTRIBUTE, showCallExtendedNotification, showCallExtendedByYouNotification , showSharedcontactNotification, RESET_STATE_CALL } from '../store/actions/video'
// import { nextProposition } from '../store/actions/video'
import ShareNumberNotirication from './ShareNumberNotification'
import SmallNotification from '../Components/SmallNotification'
import IdleTimerManager from 'react-native-idle-timer';

const {Agora} = NativeModules

const {
	FPS30,
	AudioProfileDefault,
	AudioScenarioDefault,
	Adaptive
} = Agora;


const CallModal = (props) => {
	const [time, setTime] = useState(15)
	const [callTime, setCallTime] = useState(180)
	const [callCounter, setCallCounter] = useState(null)
	const [progress, setProgress] = useState(0)
	const [progress480, setProgress480] = useState(0)
	const [stage, setStage] = useState(0)
	const [counter, setCounter] = useState(null)
	const [preeIds, setPreeIds] = useState([])
	const [uid, setUid] = useState(Math.floor(Math.random()*100))
	const [apiid, setApiid] = useState("720beab915c548c089981f5bc0e7b8bf")
	const [joinSuceed, setJoinSuceed] = useState(false)
	const [loading, setLoading] = useState('load')
	const [checkEndCall, setCheckeEndCall] = useState(1)
	const [userEnd, setUserEnd] = useState(false)
	const [extendCall, setExtendCall] = useState(false)
	const [timerHelper, setTimerHelper] = useState(180)
	const [isExtended, setIsExtedned] = useState(false)

	const [showEndCallButton, setShowEndCallButton] = useState(false)
	
	const callSocket = useSelector(state => state.video.call)
	const currentCall = useSelector(state => state.video.currentCall)
	const videoCallUid = useSelector(state => state.video.videoCallUid)

	const dispatch = useDispatch()

	// props.navigation.dispatch(resetAction);

	const config = {
		appid: apiid,
		chanelProfile: 0,
		videoEncoderConfig: {
			width: 720,
			height: 1000,
			bitrate: 1,
			frameRate: FPS30,
			orientationMode: 0
		},
		audioProfile: AudioProfileDefault,
		audioScenerio: AudioScenarioDefault,
	}


	const countDown = () => {
		setTime(time => {
			if (time > 0) {
				return time - 1
			} else {
				setCounter(counter => {
					clearInterval(counter)
					return null
				})
				return time
			}
		})
	}

	const  callCountDown = () => {
		setCallTime(callTime => {
			if (callTime > 0) {
				return (
					setProgress(((180) - callTime) * (100/(180))),
					setProgress480(((global.timerHelper) - callTime) * (100/(global.timerHelper))),
					callTime - 1
					)}
			else {
				setCallCounter(callCounter => {
					clearInterval(callCounter)
					return null
				})
				return callTime
			}
		})
	}

	const verifyPermissions = async () => {
		const result = await Permissions.askAsync(
		  Permissions.CAMERA,
		  Permissions.AUDIO_RECORDING,
		  Permissions.CONTACTS
		);
		if (result.status !== 'granted') {
		  Alert.alert(
			'Insufficient permissions!',
			'You need to grant camera permissions to use this app.',
			[{ text: 'Okay' }]
		  );
		  return false;
		}
		return true;
	  };
	
	  const checkPremission = async () => {
		const hasPermission = await verifyPermissions();
		if (!hasPermission) {
		  return;
		}
		
		RtcEngine.on('userJoined', (data) => {
			if(preeIds.indexOf(data.uid) === -1){
				setPreeIds([...preeIds,data.uid])
			}
		})
		RtcEngine.on('userOffline', (data) => {
			setPreeIds(preeIds.filter(uid=>uid!==data.uid))
			if(callTime !== 0 ) setUserEnd(true)
		})
		RtcEngine.on('joinChannelSuccess', (data)=> {
			RtcEngine.startPreview();
			setJoinSuceed(true)
		})
		RtcEngine.init(config)
		RtcEngine.joinChannel(props.chanelName, uid)
		RtcEngine.enableAudio();
		RtcEngine.muteLocalAudioStream(true)
		IdleTimerManager.setIdleTimerDisabled(true);
	}


	const endCallHandler = () => {
		RtcEngine.leaveChannel();
		setJoinSuceed(false)
		RtcEngine.destroy()
	}

	useEffect(() => {
		if (callSocket.reject_invite || callSocket.user_rejected_call) {
			setTime(15)
			setCallTime(180)
			setTimerHelper(180)
			setIsExtedned(false)
			setStage(0)
			setCounter(counter => {
				clearInterval(counter)
				return setInterval(countDown, 1000)
			})
			setCallCounter(callCounter => {
				clearInterval(callCounter)
				return setInterval(callCountDown, 1000)
			})
			setPreeIds([])
			endCallHandler()
			IdleTimerManager.setIdleTimerDisabled(false);
			dispatch({ type: RESET_STATE_CALL})
			console.log("Działa ?")
		}
	  }, [callSocket])

	const resetAction = StackActions.reset({
		index: 0,
		actions: [NavigationActions.navigate({ routeName: 'Call' })],
	});


	
	useEffect(() => {
		checkPremission()
		if(preeIds >= 1) {setStage(1), setCounter(setInterval(countDown, 1000))}
		console.log(preeIds)
	},[preeIds])

	useEffect(() => {
		setTimeout(() => setShowEndCallButton(true), 5000)
	}, [])

	useEffect(() => {
	if(callSocket.user_ended_call && callTime !== 0 ){
		setStage(4), 
		theyEndCall()
		dispatch({ type: UPDATE_CALL_ATTRIBUTE, payload: {user_ended_call : false}})
	}
	}, [ callSocket.user_ended_call , callTime])

	const addTwoMinutes = async() => {
		const res = await Api().post(`/video_calls/${videoCallUid}/extend_call`)
		if( res.status === 412) return console.log(res.data.data.errors)
		if (res.status === 200) {
			showCallExtendedByYouNotification()
			setExtendCall(true)
			console.log(res.data)
		}
	}

	useEffect(() => {
		if(extendCall ===  true && callSocket.user_extended_call )
		{
		setIsExtedned(true)
		setProgress480(0)
		showCallExtendedNotification()
		setCallTime(callTime + 300)
		global.timerHelper = callTime + 300
		console.log("Zadziałało" + callTime )
		dispatch({ type: UPDATE_CALL_ATTRIBUTE, payload: {user_extended_call : false}})
	}
	}, [callTime, extendCall, callSocket.user_extended_call])

	const callTimer = () => {
		setCallCounter(setInterval(() => {
			callCountDown()
		}, 1000))
	}

	useEffect(() => {
		if (time <= 8) setStage(2)
		if (time === 0) {setStage(3); RtcEngine.muteLocalAudioStream(false); callTimer()}	
	}, [time])

	useEffect(() => {
		if (callTime === 0){setStage(4); timesUp()}	
	}, [userEnd, callTime])


	const shareNumberHandler = async () => {
		const res = await Api().patch(`/matched_pairs/${props.chanelName}/share_contact_info`)
		if( res.status === 401) return console.log(res.data.data.errors)
		if (res.status === 200) {
			showSharedcontactNotification()
			console.log(res.data)
		}
	}

	const stage0Screen = (
		<View style={styles.screen}>
		<View style={styles.hint}>
			<Text style={{ textAlign: 'center' }}>Waiting for your partner...</Text>
		</View>
		<View style={styles.bar}>
                    <TouchableOpacity onPress={ ()=> {resetHandler(); props.hideModal();}}>
                        <Image source={PhoneRedIcon} style={styles.icon}/>
                    </TouchableOpacity>
        </View>
		</View>
	)

	const stage2Screen = (
		<View style={styles.hint}>
			<Text style={{ textAlign: 'center' }}>{props.partnerName} wants to know, would you rather be able to fly or to read minds?</Text>
		</View>
	)

	const stage3Screen = (
			<AgoraView style={styles.localUserCamera} zOrderMediaOverlay={true} showLocalVideo={true} mode={1} />
			/* remoteUid={preeIds[0]} mode={1} */
	)

	const getContent = () => {
		if (stage === 1) {
			return <Ad style={styles.ad} />
		}
		if (stage === 2) {
			return stage2Screen
		}
		if (stage === 3) {
			return stage3Screen
		}
	}

	const unmatchHandler = async () => {
		const res = await Api().delete(`/matched_pairs/${props.chanelName}`)
		if (res.status === 401) return handleErrors(res.data.data.errors)
		if (res.status === 204) return (
			Alert.alert(
			'Unmatch!',
			`You unmatched with ${props.partnerName}`,
			[{ text: 'Okay', onPress: () => {
				resetHandler()
				dispatch({ type: REMOVE_FROM_LIST, uuid: props.chanelName })
				props.hideModal()
			}}]
			))
	  }

	  const report = async (reason , reasonAlert) => {
		const payload = {
			report: {
			  reported_id: props.user_id,
			  reason: reason
			}
		  }
		  const res = await Api().post('/reports', payload)
		  if (res.status === 401) return handleErrors(res.data.data.errors)
		if (res.status === 201) return (
			Alert.alert(
			'Report!',
			`You report ${props.partnerName} for ${reasonAlert}`,
			[{ text: 'Okay', onPress: () => {resetHandler(), props.hideModal()}}]
		  ))
	  }
	


	const resetHandler = async() => {
		const res = await Api().post(`/video_calls/${videoCallUid}/finish`)
		if (res.status === 401) return handleErrors(res.data.data.errors)
			if (res.status === 201) return 
		// nextProposition()
		setTime(15)
		setCallTime(180)
		setTimerHelper(180)
		setIsExtedned(false)
		setStage(0)
		setCounter(counter => {
			clearInterval(counter)
			return setInterval(countDown, 1000)
		})
		setCallCounter(callCounter => {
			clearInterval(callCounter)
			return setInterval(callCountDown, 1000)
		})
		setPreeIds([])
		endCallHandler()
		IdleTimerManager.setIdleTimerDisabled(false);
		dispatch({ type: RESET_STATE_CALL})
	}

	const theyEndCall = async() => {
		setCallCounter(callCounter => {
			clearInterval(callCounter)
			return null
		})
		setCounter(counter => {
			clearInterval(counter)
			return null
		})
		RtcEngine.leaveChannel();
		RtcEngine.destroy()
		setStage(4);
		IdleTimerManager.setIdleTimerDisabled(false);
		setCheckeEndCall(1)
	}

	const youEndCall = async() => {
		const res = await Api().post(`/video_calls/${videoCallUid}/finish`)
		if (res.status === 401) return handleErrors(res.data.data.errors)
		if (res.status === 201) return 
		// nextProposition()
		setCallCounter(callCounter => {
			clearInterval(callCounter)
			return null
		})
		RtcEngine.leaveChannel();
		RtcEngine.destroy()
		setStage(4);
		IdleTimerManager.setIdleTimerDisabled(false);
		setCheckeEndCall(2)
	}

	const timesUp = async() => {
		setCallCounter(callCounter => {
			clearInterval(callCounter)
			return null
		})
		RtcEngine.leaveChannel();
		RtcEngine.destroy()
		setStage(4);
		IdleTimerManager.setIdleTimerDisabled(false);
		setCheckeEndCall(3)
	}

	const endCallButton = (
		<TouchableOpacity onPress={()=> { resetHandler(); props.hideModal() }}>
			<Image source={PhoneRedIcon} style={styles.icon} />
		</TouchableOpacity>
	)

	if (!joinSuceed) return (
		<View style={showEndCallButton ? styles.loaderWithButton : styles.loader}>
			{showEndCallButton ? <View style={{ height: '20%' }}/> : null}
			<ActivityIndicator size="large" color="black"/>
			{showEndCallButton ? endCallButton : null}
		</View>
	)
	

  return (
        <Modal visible={props.visible} animationType="fade">
			<ShareNumberNotirication/>
			{stage === 0 ? stage0Screen : (stage == 4 ?<EndCallView 
							partnerName={props.partnerName}
							phonePartner={props.phonePartner}
							checkEndCall={checkEndCall} 
							option={() => {setCheckeEndCall(4)}} 
							option2={() => {shareNumberHandler(); setCheckeEndCall(7)}} 
							option3={() => {setCheckeEndCall(6)}} 
							option4={() => {unmatchHandler()}}
							reportOption1={() => {report("inappropriate_behavior", "inappropriate behavior")}}
							reportOption2={() => {report("general_meanness", "general meanness")}}
							nameOfCloseButton={"Close"} 
							onPress={() => {resetHandler(), props.hideModal()}}/> : 
						<View style={styles.screen}>
                { stage === 3 ? <View style={styles.cameraCall}>
                        <AgoraView style={styles.localUserCamera} zOrderMediaOverlay={true} showLocalVideo={true} mode={1}/>
                    </View> : <View style={styles.header}>
                    <View style={styles.headerContent}>
                        <View style={styles.helperCounter}>
                        <Text style={styles.text}>Calling now, meet {props.partnerName} in:</Text>
                        <Counter time={time} />
                        </View>
                    </View>
                    <View style={styles.camera}>
                        <AgoraView style={styles.localUserCamera} zOrderMediaOverlay={true} showLocalVideo={true} mode={1}/>
                    </View>
                </View>}
                {	stage === 3 ?
                    <AgoraView style={styles.callingUserCamera} remoteUid={preeIds[0]} mode={1} mode={1} />
                    :
                <View style={styles.content}>
                    {getContent()}
                </View>
                }
                { stage === 3 ? <View style={styles.barStage3}>
								<TouchableOpacity onPress={ ()=> {youEndCall()}}>
                        <Image source={PhoneRedIcon} style={styles.icon}/>
                    </TouchableOpacity>

					<TouchableOpacity onPress={() => addTwoMinutes()}>
						<ProgressCircle
							percent={isExtended ? progress480 : progress}
							radius={25}
							borderWidth={50}
							shadowColor={isExtended ? (progress480 <=50 ? '#59c49e' : (progress480 >=75 ? "#ed5143" : "orange") ): (progress <=50 ? '#59c49e' : (progress >=75 ? "#ed5143" : "orange") ) }
							color={colors.lightYellow}
							outerCircleStyle={styles.progressCircle}
						/>
					</TouchableOpacity>
                    <TouchableOpacity onPress={() => {shareNumberHandler()}}>
                    <View style={styles.digits}>
                        <Text style={styles.textDigits}>Share #</Text>
                    </View>
                    </TouchableOpacity>
                </View> :
                <View style={styles.bar}>
                    <TouchableOpacity onPress={ ()=> {resetHandler(); props.hideModal();}}>
                        <Image source={PhoneRedIcon} style={styles.icon}/>
                    </TouchableOpacity>
                </View>
                }			
            </View>)}
					<SmallNotification />
        </Modal>
  )
  }

const styles = StyleSheet.create({

	screen: {
    flex: 1,
    alignItems: 'center',
		backgroundColor: colors.lightBlue,
		paddingTop: 30, // %TODO get navbar height
	},
	header: {
		height: '20%',
		width: '100%',
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between'
	},
	content: {
		height: '70%',
		width: '100%',
	},
	bar: {
		position: "absolute", 
		zIndex: 1,
		bottom: 0,
		height: 90,
		backgroundColor: colors.lightBlue,
		width: '100%',
		flexDirection: 'row',
		justifyContent: 'space-evenly',
		alignItems: 'center'
	},
	barStage3:{
		position: "absolute", 
		zIndex: 1,
		bottom: 0,
		height: 90,
		backgroundColor: colors.lightYellow,
		width: '100%',
		flexDirection: 'row',
		justifyContent: 'space-evenly',
		alignItems: 'center'
	},
	ad: {
		width: Dimensions.get('window').width,
		height: '100%',
	},
	cameraCall: {
		position: "absolute",
		zIndex: 2,
		margin: 10,
		top: 27,
		right: 5,
		width: 160,
		height: 160
	},
	camera: {
		margin: 10,
		flex: 1
	},
	localUserCamera:{
		alignItems: 'center',
		justifyContent: 'center',
		width: '100%',
		height: '100%',
	},
	callingUserCamera: {
		position: "absolute",
		zIndex: 0,
		width: "100%",
		height: "100%"
	},
	headerContent: {
		alignItems: 'center',
		justifyContent: 'center',
		flex: 2,
	},
	helperCounter:{
		width: 140,
		alignItems: "center",
		justifyContent: "center"
	},
	text: {
		paddingBottom: 20,
		fontWeight: "500",
		fontSize: 20
	},
	hint: {
		alignItems: 'center',
		justifyContent: 'center',
		height: '100%',
		width: '100%',
		paddingHorizontal: 20
	},
	icon: {
		width: 53,
		height: 53
	},
	timer:{
		width: 53,
		height: 53,
		borderRadius: 50,
		backgroundColor: "#59c49e",
		borderWidth: 2,
		borderColor: 'black',
		position: 'relative'
	},
	timerIndicator: {
		backgroundColor: 'black',
		width: 3,
		height: 22,
		left: 22,
		borderRadius: 4,
		top: -2
	},
	digits: {
		width: 53,
		height: 53,
		borderRadius: 50,
		alignItems: "center",
		justifyContent: "center",
		borderWidth: 2,
		backgroundColor: colors.lightBlue
	},
	textDigits:{
		textAlign: 'center',
		fontWeight: 'bold',
		marginHorizontal: 1,
		marginTop: 4
  },
  loaderWithButton: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'space-between',
		backgroundColor: colors.lightBlue,
		paddingBottom: 10
	},
	loader: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: colors.lightBlue,
	},
	progressCircle: {
		borderWidth: 1.5,
		borderColor: 'black',
		overflow: "hidden"
	}
})

export default CallModal