import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native'
import Text from './Text'
import PropTypes from 'prop-types'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import colors from '../assets/colors'

const GenderSelect = props => {

  const colorFor = (gender) => {
    if (props.variant === 'color') {
      return gender === props.value ? colors.yellowItems : '#444'
    }
    if (props.variant === 'greyscale') {
      return gender === props.value ? 'black' : '#888'
    }
  }

  const thirdGender = (
    <TouchableOpacity style={styles.genderSelect} onPress={() => props.onChange('both')}>
      <MaterialCommunityIcons name="gender-male-female" size={props.iconSize} color={colorFor('both')}/>
      <Text>Everyone</Text>
    </TouchableOpacity>
  )

  return (
    <View style={{ ...styles.container, ...props.style }}>
      <TouchableOpacity style={styles.genderSelect} onPress={() => props.onChange('male')}>
        <MaterialCommunityIcons name="gender-male" size={props.iconSize} color={colorFor('male')}/>
        <Text>Male</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.genderSelect} onPress={() => props.onChange('female')}>
        <MaterialCommunityIcons name="gender-female" size={props.iconSize} color={colorFor('female')}/>
        <Text>Female</Text>
      </TouchableOpacity>
      {props.thirdGender ? thirdGender : null}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'space-evenly',
    flexDirection: 'row',
  },
  genderSelect: {
    marginTop: 10,
    width: 100,
    justifyContent: "center",
    alignItems: "center"
  }
})

GenderSelect.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.oneOf(['female', 'male', 'both']),
  style: PropTypes.object,
  color: PropTypes.string,
  iconSize: PropTypes.number,
  variant: PropTypes.oneOf(['greyscale', 'color']),
  thirdGender: PropTypes.bool
}

GenderSelect.defaultProps = {
  onChange: () => {},
  value: 'female',
  style: {},
  iconSize: 40,
  varinat: 'color',
  thirdGender: true
}

export default GenderSelect
