import React, { useState, useEffect, useRef } from 'react'
import {View, Modal, StyleSheet, Dimensions, Platform, Image, TouchableOpacity, Alert} from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { 
  HIDE_NOTIFICATION,
  SET_NOTIFICATION_DATA,
  SET_CURRENT_CALL,
  SET_VIDEO_CALL_UID,
  UPDATE_CALL_ATTRIBUTE,
  SET_SHARE_CONTACT_DATA,
  RESET_CALL,
  RESET_STATE_CALL
} from '../store/actions/video'
import Text from './Text'
import colors from '../assets/colors'
import rejectIcon from '../assets/icnos/sluchawka-czerwona.png'
import acceptIcon from '../assets/icnos/sluchawka-zielona.png'
import env from '../env'
import Api from '../Api'
import store from '../store/store'
import Animated, { Easing } from 'react-native-reanimated'

const TIMEOUT = 15

const dimensions = {
  notificationHeight: Platform.OS === 'ios' ? 200 : 150,
  additionalPaddingTop: Platform.OS === 'ios' ? 50 : 0,
  elipsisHeight: 40,
  containerHeight: (Platform.OS === 'ios' ? 200 : 150) + 20
}

const CallNotification = (props) => {
  const showNotification = useSelector(state => state.video.showNotification)
  const notificationData = useSelector(state => state.video.notificationData)
  const readyMode = useSelector(state => state.video.readyMode)
  const token = useSelector(state => state.auth.token)
  const call = useSelector(state => state.video.call)
  const videoCallUid = useSelector(state => state.video.videoCallUid)
  const [timer, setTimer] = useState(null)
  const dispatch = useDispatch()
  const slideAnimation = useRef(new Animated.Value(-dimensions.containerHeight)).current

  const slideDown = () => {
    Animated.timing(slideAnimation, {
      toValue: 0,
      duration: 750,
      easing: Easing.cubic
    }).start()
  }

  const slideUp = () => {
    Animated.timing(slideAnimation, {
      toValue: -dimensions.containerHeight,
      duration: 750,
      easing: Easing.cubic
    }).start()
  }

  const throwError = (message) => {
    Alert.alert('Error', message, [{ text: 'Okay' }])
    throw new Error(message)
  }

  const hide = () => {
    slideUp()
    setTimeout(() => {
      dispatch({ type: HIDE_NOTIFICATION })
      dispatch({ type: SET_NOTIFICATION_DATA, payload: {} })
    }, 1000)
  }

  useEffect(() => {
    if (showNotification) slideDown()
  }, [showNotification])

  const startCall = () => {
    const payload = {
      uid: notificationData.uid,
      started: true,
      id: notificationData.id,
      name: notificationData.name,
      phone: notificationData.phone
    }

    dispatch({type : SET_SHARE_CONTACT_DATA, payload: {name: notificationData.name, phone : notificationData.phone}})
    dispatch({ type: SET_CURRENT_CALL, payload })
    hide()
  }

  useEffect(() => {
    if (!token || !readyMode) slideUp()
  }, [token, readyMode])

  useEffect(() => {
    (async () => {
      if (readyMode || !call.user_calling || !videoCallUid) return

      dispatch({ type: UPDATE_CALL_ATTRIBUTE, payload: { user_calling: false } })
      const res = await Api().post(`/video_calls/${videoCallUid}/reject`)
      console.log(`[CALL FLOW] Rejecting call after ReadyMode change, received ${res.status}`)
    })()
  }, [readyMode])

  useEffect(() => {
    if (!showNotification) {
      if (timer) clearInterval(timer)
      setTimer(null)
      return
    }
    setTimer(setTimeout(hide, TIMEOUT * 1000))
  }, [showNotification])

  useEffect(() => {
    if (!call.reject_invite && !call.user_rejected_call) return
    if (call.reject_invite) {
      dispatch({ type: UPDATE_CALL_ATTRIBUTE, payload: { reject_invite: false } })
    }
    if (call.user_rejected_call) {
      dispatch({ type: UPDATE_CALL_ATTRIBUTE, payload: { user_rejected_call: false } })
    }
    hide()
  }, [call.reject_invite, call.user_rejected_call])

  const acceptHandler = async () => {
    if (!call.user_calling) {
      console.log('[CALL FLOW] You\'re the first one calling')
      const res = await Api().post(`/video_calls/${notificationData.id}`)
      console.log(`[CALL FLOW] Accepting user, received ${res.status}`)
      if (res.status === 201) {
        dispatch({type: SET_VIDEO_CALL_UID, payload : res.data.data.id})
        startCall()
      }
      if (res.status === 412) {
          console.log(res.data.data.errors[0].key)
        if (res.data.data.errors[0].key === 'user.errors.video_calls.someone_else_calls') {
          console.log('[CALL FLOW] Changing to pick up')
          setTimeout(async () => {
            dispatch({ type: UPDATE_CALL_ATTRIBUTE, payload: { user_calling: false } })
            const uid = store.getState().video.videoCallUid
            const res = await Api().post(`/video_calls/${uid}/pick_up`)
            console.log(`[CALL FLOW] Accepting user, received ${res.status}`)
            if (res.status === 200) {
              startCall()
            }
            if (res.status === 412) {
              console.log(res.data.data.errors)
              throwError(res.data.data.errors[0].message)
            }
            if (res.status === 404) {
              throwError('Video call not found')
            }
          }, 500)
        }
      }
      if (res.status === 404) {
        throwError('Matched user not found')
      }
    }
    else {
      console.log('[CALL FLOW] Matched user was first one calling')
      dispatch({ type: UPDATE_CALL_ATTRIBUTE, payload: { user_calling: false } })
      const res = await Api().post(`/video_calls/${videoCallUid}/pick_up`)
      console.log(`[CALL FLOW] Accepting user, received ${res.status}`)
      if (res.status === 200) {
        startCall()
      } 
      if (res.status === 412) {
        throwError(res.data.data.errors[0].message)
      }
      if (res.status === 404) {
        throwError('Video call not found')
      }
    }
    dispatch({ type: RESET_STATE_CALL })
  }

  const rejectHandler = async () => {
    hide()
    if (!call.user_calling) {
      console.log('[CALL FLOW] I reject when user is NOT calling')
      const res = await Api().post(`/matched_pairs/${notificationData.uid}/reject_invite`)
      console.log(`[CALL FLOW] Rejecting user, received ${res.status}`)
      if (res.status === 412) {
        throwError(res.data.data.errors[0].message)
      } else if (res.status === 404) {
        throwError('Matched user not found')
      }
      console.log('[CALL FLOW] Rejected invite, waiting in case matched user calls')
      setTimeout(async () => {
        const user_calling = store.getState().video.call.user_calling
        const videoCallUid = store.getState().video.videoCallUid
        if (user_calling && videoCallUid) {
          dispatch({ type: UPDATE_CALL_ATTRIBUTE, payload: { user_calling: false } })
          Api().post(`/video_calls/${videoCallUid}/reject`)
        }
      }, 2000)
    } else {
      console.log('[CALL FLOW] I reject when user is calling')
      dispatch({ type: UPDATE_CALL_ATTRIBUTE, payload: { user_calling: false } })
      const res = await Api().post(`/video_calls/${videoCallUid}/reject`)
      console.log(`[CALL FLOW] Rejecting user, received ${res.status}`)
      if (res.status === 412) {
        throwError(res.data.data.errors[0].message)
      }
      if (res.status === 404) {
        throwError('Video call not found')
      }
    }
    dispatch({ type: RESET_CALL })
  }

  const buttons = (
    <View style={styles.callChoiceBox}>
      <TouchableOpacity onPress={rejectHandler}>
        <Image source={rejectIcon} style={styles.icon}/>
      </TouchableOpacity>
      <TouchableOpacity onPress={acceptHandler}>
        <Image source={acceptIcon} style={styles.icon}/>
      </TouchableOpacity>
    </View>
  )

  const url = () => {
    if (!notificationData.thumb) return
    return notificationData.thumb.replace('localhost', env.hostname)
  }

  return (
    <Animated.View style={{ ...styles.container, top: slideAnimation }}>
      <View style={{ ...styles.elipsis, ...styles.shadow }} />
      <View style={{...styles.rectangle, ...(Platform.OS === "android" ? styles.shadow : null ) }}>
        <Image source={{ uri: url() }} style={styles.img}/>
        <View style={styles.callInfo}>
          <Text style={styles.text}>Ready for a videodate with {notificationData.name}?</Text>
          {buttons}
        </View>
      </View>
    </Animated.View>
  )
}

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    width: "100%",
    alignItems: 'center'
  },
  rectangle: {
    width: '100%',
    flexDirection: 'row',
    backgroundColor: colors.lightYellow,
    height: dimensions.notificationHeight,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: dimensions.additionalPaddingTop,
    elevation: 4
  },
  elipsis: {
    position: 'absolute',
    top: dimensions.notificationHeight - dimensions.elipsisHeight / 2,
    width: '10%',
    backgroundColor: colors.lightYellow,
    height: dimensions.elipsisHeight,
    borderRadius: dimensions.elipsisHeight,
    transform: [{ scaleX: 10 }]
  },
  img: {
    width: 120,
    height: 120,
    borderRadius: 60,
    margin: 10,
    marginTop: 10 + dimensions.elipsisHeight / 2
  },
  callInfo: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 10,
    marginTop: 10 + dimensions.elipsisHeight / 2
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
    marginBottom: 20,
  },
  callChoiceBox:{
    flexDirection: "row",
    width: 150,
    justifyContent: "space-between"
  },
  icon: {
    width: 45,
    height: 45,
  },
  shadow: {
    elevation: 4,
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
  }
})

export default CallNotification
