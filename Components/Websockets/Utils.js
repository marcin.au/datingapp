const DEBUG = true

export const logger = (message) => {
  if (!DEBUG) return
  console.log('[WEBSOCKET]', message)
}

export const knownNotifications = [
  'video_call_timed_out',
  'user_calling',
  'user_ended_call',
  'user_extended_call',
  'user_picked_up_call',
  'user_rejected_call',
  'shared_contact_info',
  'ready_mode_changed',
  'call_invite',
  'matched_pair_created'
]