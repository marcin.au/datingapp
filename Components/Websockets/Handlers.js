
import store from '../../store/store'
import * as videoActions from '../../store/actions/video'
import * as matchActions from '../../store/actions/match'
import * as websocketActions from '../../store/actions/websocket'

const sendStatus = (attribute) => {
  store.dispatch({ type: videoActions.UPDATE_CALL_ATTRIBUTE, payload: { [attribute]: true } })
}

const hideNotification = (userId) => {
  const userIdFromNotification = store.getState().video.notificationData.id
  if (!userIdFromNotification || userIdFromNotification !== userId) return

  store.dispatch({ type: videoActions.HIDE_NOTIFICATION })
  store.dispatch({ type: videoActions.SET_NOTIFICATION_DATA, payload: {} })
}

export const onEveryMessage = (payload) => {
  const { name, data } = payload.message
  store.dispatch({ type: websocketActions.NEW_MESSAGE, name, data })
}

export const ready_mode_changed = (payload) => {
  const { data } = payload.message.data.user
  const { id } = data
  const status = data.attributes.ready_mode
  store.dispatch({ type: matchActions.UPDATE_READY_MODE, payload: { id, status } })

  hideNotification(id)
}

export const matched_pair_created = (response) => {
  const { data } = response.message
  const user = data.matched_pair.data.attributes.matched_user
  const account = user.included[0].attributes
  const payload = {
    uuid: data.matched_pair.data.id,
    profileId: user.data.id,
    avatar: user.data.attributes.main_image_thumb_url,
    name: account.name,
    age: account.age,
    work: account.work,
    lives_in: account.lives_in,
    ready: user.data.attributes.ready_mode
  }
  store.dispatch({ type: matchActions.APPEND_TO_MATCHED_LIST, payload })
  store.dispatch({ type: videoActions.SET_SMALL_NOTIFICATION, context: 'new-match' })
  store.dispatch({ type: videoActions.SET_SMALL_NOTIFICATION_DATA, payload })
}

export const call_invite = (response) => {
  if (store.getState().video.showNotification) return

  const { data } = response.message.data.matched_pair
  const user = data.attributes.matched_user
  const payload = {
    uid: data.id,
    id: user.data.id,
    thumb: user.data.attributes.main_image_thumb_url,
    name: user.included[0].attributes.name,
    phone: user.included[0].attributes.phone
  }
  const gender = user.included[0].attributes.gender
  const name = user.included[0].attributes.name
  console.log('Invite from', payload.name)
  store.dispatch({ type: videoActions.SET_NOTIFICATION_DATA, payload })
  store.dispatch({ type: videoActions.SHOW_NOTIFICATION })
  store.dispatch({ type: videoActions.SET_REJECT_NAME,  name})
  store.dispatch({ type: videoActions.SET_MATCHED_USER_GENDER, gender })
}

export const matched_pair_destroyed = (payload) => {
  const data = payload.message.data
  const matchedPair = data.matched_pair.data
  store.dispatch({ type: matchActions.REMOVE_FROM_LIST, uuid: matchedPair.id })
}

export const video_call_timed_out = (payload) => {
  const { name } = payload.message
  sendStatus(name)
}

export const user_calling = (payload) => {
  const videoCallUid = payload.message.data.video_call.data.id
  const { name } = payload.message
  store.dispatch({ type: 'SET_VIDEO_CALL_UID', payload: videoCallUid })
  sendStatus(name)
}

export const user_ended_call = (payload) => {
  const { name } = payload.message
  sendStatus(name)
}

export const user_extended_call = (payload) => {
  const { name } = payload.message
  sendStatus(name)
}

export const user_picked_up_call = (payload) => {
  const { name } = payload.message
  sendStatus(name)
}

export const user_rejected_call = (payload) => {
  const { name } = payload.message
  sendStatus(name)
}

export const reject_invite = (payload) => {
  const { name } = payload.message
  sendStatus(name)
}

export const shared_contact_info = (payload) => {
  const { name } = payload.message
  sendStatus(name)
}
