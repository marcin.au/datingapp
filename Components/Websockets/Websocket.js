import { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import env from '../../env'
import * as Handlers from './Handlers'
import * as MessageTypes from './MessageTypes'
import { logger, knownNotifications } from './Utils'
import store from '../../store/store'

const Websocket = () => {
  const [socket, setSocket] = useState(null)
  const token = useSelector(state => state.auth.token)

  const subscribeToChannel = (socket, channel) => {
    const payload = JSON.stringify({
      command: 'subscribe',
      identifier: JSON.stringify({ channel })
    })
    socket.send(payload)
  }

  const disconnect = () => {
    logger('Disconnecting')
    if (!socket) return
    socket.removeEventListener('message')
    socket.removeEventListener('open')
    socket.removeEventListener('close')
    socket.removeEventListener('error')
    socket.close()
    setSocket(null)
  }

  const restart = () => {
    // Don't remove getState() here
    const token = store.getState().auth.token
    if (!token) return
    logger('Restart')
    disconnect()
    connect()
  }

  const messageHandler = (event) => {
    const payload = JSON.parse(event.data)
    const { type } = payload

    if (MessageTypes[type]) return MessageTypes[type](payload)

    if (!payload.message) return MessageTypes.unknown(payload)

    const { name } = payload.message
    logger(`New message - ${name}`)
    if (knownNotifications.includes(name)) Handlers.onEveryMessage(payload)
    if (Handlers[name]) return Handlers[name](payload)

    return MessageTypes.unknown(payload)
  }
  
  const connect = async () => {
    logger('Create new socket')
    const url = `ws://${env.host}/cable`

    setSocket(() => {
      const socket = new WebSocket(url, token)

      socket.onopen = () => subscribeToChannel(socket, 'WebNotificationChannel')
      socket.onmessage = messageHandler
      socket.onclose = () => setTimeout(restart, 5000)
      socket.onerror = () => socket.close()

      return socket
    })    
  }

  useEffect(() => {
    if (!token && socket) disconnect()
    if (token && !socket) connect() 
  }, [token])

  return null
}

export default Websocket
