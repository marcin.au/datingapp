import { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import * as callsActions from '../../store/actions/calls'
import * as websocketActions from '../../store/actions/websocket'

const Worker = () => {
  const dispatch = useDispatch()
  const messages = useSelector(state => state.websocket)
  const callStack = useSelector(state => state.calls.callStack)
  const currentCall = useSelector(state => state.calls.current)

  const findItemIndexInCallStack = (candidate) => {
    if (callStack.length === 0) return -1

    return callStack.findIndex(item => {
      if (!item) throw new Error('There is invalid item in call stack')

      if (candidate.matchedPairId !== item.matchedPairId) return false
      if (candidate.matchedUserId !== item.matchedUserId) return false
      if (candidate.matchedUserUuid !== item.matchedUserUuid) return false
      return true
    })
  }

  const checkIfItemIsCurrentCall = (candidate) => {
    if (!currentCall) return false
    if (candidate.matchedPairId !== currentCall.matchedPairId) return false
    if (candidate.matchedUserId !== currentCall.matchedUserId) return false
    if (candidate.matchedUserUuid !== currentCall.matchedUserUuid) return false
    return true
  }

  useEffect(() => {
    if (!messages.call_invite.status) return

    const message = messages.call_invite.data
    const matchedPair = message.matched_pair
    const matchedUser = matchedPair.data.attributes.matched_user
    const matchedUserAccount = matchedUser.included[0].attributes
    const payload = {
      matchedPairId: matchedPair.data.id,
      matchedUserId: matchedUser.data.id,
      matchedUserUuid: matchedUser.data.attributes.uuid,
      matchedUserData: {
        avatar: matchedUser.data.attributes.main_image_thumb_url,
        ...matchedUserAccount
      }
    }
    const itemIndexInCallStack = findItemIndexInCallStack(payload)
    const itemIsCurrentCall = checkIfItemIsCurrentCall(payload)

    if (itemIndexInCallStack !== -1 && callStack.length > 0 && !itemIsCurrentCall) {
      dispatch({ type: callsActions.MOVE_TO_TOP, index: itemIndexInCallStack })
    }
    else if (!itemIsCurrentCall) {
      dispatch({ type: callsActions.PUSH, payload })
    }
    dispatch({ type: websocketActions.RECEIVE_MESSAGE, name: 'call_invite' })
  }, [messages.call_invite])

  useEffect(() => {
    if (callStack.length !== 0 && !currentCall) {
      dispatch({ type: callsActions.POP })
    }
  }, [callStack, currentCall])

  return null
}

export default Worker
