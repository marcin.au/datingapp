import { logger } from './Utils'

export const welcome = () => {
  logger('Connected')
}

export const ping = (payload) => {
  // logger(`Ping ${payload.message}`)
}

export const unknown = (payload) => {
  const name = payload.message ? payload.message.name : 'none'
  logger(`Unknown message - type: ${payload.type} - name: ${name}`)
}

export const confirm_subscription = (payload) => {
  logger('Channel subscribed')
}
