import { useEffect } from 'react'
import * as Permissions from 'expo-permissions'

const ContactPremission = (props) => { 

    useEffect(() => {
        const verifyPermissions = async () => {
            const result = await Permissions.askAsync(
                Permissions.CONTACTS
              );
              if (result.status !== 'granted') {
                Alert.alert(
                  'Insufficient permissions!',
                  'You need to grant contact permissions to use this app.',
                  [{ text: 'Okay' }]
                );
                return
              }
        }
        verifyPermissions()
      }, [])


  return null
}

export default ContactPremission