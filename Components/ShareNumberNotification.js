import React, {useState, useEffect} from 'react'
import {View, ScrollView, Image, StyleSheet, Dimensions, TouchableOpacity, Platform, Modal, Alert} from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { UPDATE_CALL_ATTRIBUTE, SET_SHARE_CONTACT_DATA } from '../store/actions/video'
import colors from '../assets/colors'
import Text from './Text'
import * as Contacts from 'expo-contacts';
import { UPDATE_CONTACT, fetchMatchedList } from '../store/actions/match'

const ShareNumberNotification = props => {

    const [showModal, setShowModal] = useState(false)
    const [phone, setPhone] = useState('')
    const [name, setName] = useState('')

    const shareNumber = useSelector(state => state.video.call.shared_contact_info)
    const currentCall = useSelector(state => state.video.shareContact)
    const userId = useSelector(state => state.video.currentCall.id)
    const dispatch = useDispatch()

    useEffect(() => {
        setShowModal(shareNumber)
        setPhone(currentCall.phone)
        setName(currentCall.name)

    }, [shareNumber, currentCall.phone, currentCall.name])

    useEffect(() => {
        if (!shareNumber) return
        dispatch(fetchMatchedList())
    }, [shareNumber])

    const closeModalHandler = () => {
        setShowModal(false)
        dispatch({type: UPDATE_CALL_ATTRIBUTE, payload: { shared_contact_info : false}})
        dispatch({type : SET_SHARE_CONTACT_DATA, payload: {name: null, phone : null}})
    }

    const saveContact = async () => {
        const contactIOS = {
            [Contacts.Fields.FirstName]: `Dating App - ${name}`,
            [Contacts.Fields.PhoneNumbers]: [{number: phone}]
        }
        const contactAndroid = {
            [Contacts.Fields.FirstName]: `Dating App - ${name}`,
            [Contacts.Fields.PhoneNumbers]: [{ label : 'mobile', number: `${phone}` }]
        }
        Alert.alert(
			'Save Contact!',
			`You save contact as Dating App - ${name}`,
			[{ text: 'Okay', onPress: async() => {
                if(Platform.OS === "ios"){
                    const containerId = await Contacts.getDefaultContainerIdAsync()
                    await Contacts.addContactAsync(contactIOS, containerId)
                }
                else {
                    await Contacts.addContactAsync(contactAndroid)
                }
                closeModalHandler()
			}}]
			)
    }
    
    
    return (
        <Modal visible={showModal}>
            <View style={styles.screen}>
                <View style={styles.content}>
                    <Text style={styles.text}>
                        You and {name} have exchanged numbers...
                        does someone have a crush?
                    </Text>
                    <View style={styles.phonenumberBox}>
                        <Image source={require('../assets/kartka.png')} style={styles.phoneNumberCard}/>
                        <View style={styles.phoneNumber}>
                            <Text style={styles.textPhone}>{name}</Text>
                            <Text style={styles.textPhone}>{phone}</Text>
                        </View>
                    </View>
                    <TouchableOpacity style={styles.saveContact} onPress={() => saveContact()}>
                        <Text style={styles.text}>Save Contact info?</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.saveContact} onPress={() => closeModalHandler()}>
                        <Text style={{...styles.text, color: "green"}}>Go Back</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>

);
}

const styles = StyleSheet.create({
    screen: {
        position: "absolute",
        height: "100%",
        width: '100%',
        backgroundColor: colors.lightBlue,
        justifyContent: 'center',
        alignItems: "center",
    },
    content: {
        width: "60%",
        justifyContent: "center",
        alignItems: "center"
    },
    text: {
        fontSize: 20,
        fontWeight: "700",
        textAlign: 'center',
        marginVertical: 15
    },
    phonenumberBox: {
        width: Dimensions.get("window").width,
        height: 300,
        marginVertical: 30,
        justifyContent: "center",
        alignItems: "center"
    },
    phoneNumberCard: {
        width: "100%",
        height: "100%",
        marginVertical: 30
    },
    phoneNumber: {
        position: 'absolute',
        bottom: 0,
        top: 80,
        left: 0,
        right: 0,
    },
    textPhone: {
        fontFamily: Platform.OS == "ios" ? "Bradley Hand" : "monospace",
        fontSize: 40,
        fontWeight: "bold",
        textAlign: "center",
        marginVertical: 10
    },
    saveContact: {
        marginTop: 10
    },
})

export default ShareNumberNotification