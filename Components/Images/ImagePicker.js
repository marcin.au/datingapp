import React, { useState } from 'react'
import { View, Image, StyleSheet, TouchableOpacity, Text, ActivityIndicator, Dimensions } from 'react-native'
import PropTypes from 'prop-types'
import Api from '../../Api'
import Popup from './Popup'
import colors from '../../assets/colors'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { matchImageSizeToScreen } from '../../utils'

const ImagePicker = props => {
  const [showModal, setShowModal] = useState(false)

  let sizes = {
    thumb: props.thumb,
    medium: props.medium,
    big: props.big,
    orig: props.orig
  }

  for (let key of Object.keys(sizes)) {
    if (sizes[key]) {
      sizes[key] = sizes[key].replace('localhost', Api().hostname)
    }
  }

  const empty = <Text style={styles.plus}>+</Text>
  const image = <Image style={styles.image} source={{ uri: sizes.thumb }} />
  const loading = <ActivityIndicator size="large" color="blakc"/>

  const modal = (
    <Popup
      url={sizes[matchImageSizeToScreen()]}
      hide={() => setShowModal(false)}
      deleteHandler={props.deleteHandler}
      setAsMainHandler={props.setAsMainHandler}
      imagesCount={props.imagesCount}
    />
  )

  const mainIcon = (
    <FontAwesome name="user-circle-o" style={styles.mainIcon} size={24} color={colors.lightYellow}/>
  )

  const onPress = sizes.thumb ? () => setShowModal(true) : props.uploadHandler

  return (
    <React.Fragment>
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={onPress}
      >
        <View style={props.main ? styles.mainImage : styles.imagePreview}>
          {sizes.thumb ? image : (props.loading ? loading : empty)}
          {props.main ? mainIcon : null}
        </View>
      </TouchableOpacity>
      {showModal ? modal : null}
    </React.Fragment>

  )
}
  
const styles = StyleSheet.create({
  imagePreview: {
    width: 105,
    height: 105,
    backgroundColor: "white",
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    marginVertical: 5,
    marginHorizontal: 5,
    overflow: "hidden"
  },
  mainImage: { // %TODO Make it shorter
    width: 105,
    height: 105,
    backgroundColor: "white",
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    marginVertical: 5,
    marginHorizontal: 5,
    overflow: "hidden",
    borderWidth: 2,
    borderColor: colors.lightYellow,
    position: 'relative'
  },
  plus: {
    fontSize: 35,
    fontWeight: "bold",
    color: "grey"
  },
  image: {
    width: '100%',
    height: '100%',
  },
  mainIcon: {
    position: 'absolute',
    zIndex: 10,
    right: 4,
    bottom: 4
  }
})

// TODO fill those
ImagePicker.propTypes = {
  id: PropTypes.string,
  main: PropTypes.bool,
  loading: PropTypes.bool,
  uploadHandler: PropTypes.func.isRequired,
  deleteHandler: PropTypes.func.isRequired,
  setAsMainHandler: PropTypes.func.isRequired,
  position: PropTypes.number
}

export default ImagePicker
