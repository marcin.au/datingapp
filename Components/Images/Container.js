import React, { useState, useEffect } from 'react'
import { View, StyleSheet, ActivityIndicator } from 'react-native'
import ImagePicker from './ImagePicker'
import * as Picker from 'expo-image-picker';
import * as Logic from './Logic'
import { useSelector } from 'react-redux';

const UserImages = props => {
  const [images, setImages] = useState([{}, {}, {}, {}, {}, {}])
  const [_, setPermission] = useState(false)
  const [loading, setLoading] = useState(true)
  const currentUser = useSelector(state => state.auth.currentUser)

  const getImagesCount = () => images.filter(item => item.hasImage).length

  useEffect(() => {
    (async () => {
      setPermission(await Logic.verifyPermissions())
      const receivedImages = await Logic.getImages()
      const images = Logic.appendEmptyImages(receivedImages)
      setImages(images)
      setLoading(false)
    })()
  }, [])

  useEffect(() => {
    props.onImageCountChange(getImagesCount())
  }, [images])

  const uploadImageHandler = async (index) => {
    const image = await Picker.launchImageLibraryAsync({
      mediaTypes: Picker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [1, 1],
      quality: 1
    })

    if (image.cancelled) return

    Logic.updateAttributeByIndex('loading', true, index, setImages)
    const result = await Logic.uploadImage(image.uri, currentUser.id)
    Logic.updateAttributeByIndex('loading', false, index, setImages)

    if (!result) return Logic.alert('Couldn\'t upload image')
    Logic.appendImage(result, setImages, index, image.uri)

    Logic.placeInOrder(setImages)
  }

  const deleteHandler = async (id, index) => {
    if (getImagesCount() === 1) {
      Logic.alert('You cannot remove your last image')
      return
    }
    const tempUrl = images[index].thumb
    Logic.updateAttributeByIndex('thumb', null, index, setImages)
    Logic.updateAttributeByIndex('loading', true, index, setImages)
    const result = await Logic.deleteImage(id)
    Logic.updateAttributeByIndex('loading', false, index, setImages)
    if (!result) {
      Logic.alert('Image couldn\'t be deleted')
      Logic.updateAttributeByIndex('thumb', tempUrl, index, setImages)
      return
    }
    Logic.updateAttributeByIndex('main', false, index, setImages)
    Logic.updateAttributeByIndex('hasImage', false, index, setImages)

    const newMain = Logic.findMainImage(index, images)

    setImages(images => images.map(item => ({ ...item, main: false })))

    if (newMain === -1) return
    Logic.updateAttributeByIndex('main', true, newMain, setImages)

    Logic.placeInOrder(setImages)
  }

  const setAsMainHandler = async (id, index) => {
    const result = Logic.setAsMain(id)
    if (result) {
      Logic.unMarkMainImage(setImages)
      Logic.updateAttributeByIndex('main', true, index, setImages)
    }
  }

  const mapImages = (arr, startPosition) => {
    return arr.map((image, index) => (
      <ImagePicker
        key={`image-${index}`}
        position={index + startPosition}
        {...image}
        uploadHandler={() => uploadImageHandler(index + startPosition)}
        deleteHandler={() => deleteHandler(image.id, index + startPosition)}
        setAsMainHandler={() => setAsMainHandler(image.id, index + startPosition)}
        imagesCount={props.imagesCount}
      />
    ))
  }

  const topRow = mapImages(images.slice(0,3), 0)
  const bottomRow = mapImages(images.slice(3,6), 3)

  const loader = (
    <View style={styles.spinner}>
      <ActivityIndicator size="large" color="black" />
    </View>
  )

  const content = (
    <View>
      <View style={styles.container}>
        {topRow}
      </View>
      <View style={styles.container}>
        {bottomRow}
      </View>
    </View>
  )

  return loading ? loader : content
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 115
  },
  spinner: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    height: 230
  }
})

export default UserImages
