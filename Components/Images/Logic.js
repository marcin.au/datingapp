import * as Permissions from 'expo-permissions'
import MD5 from 'react-native-md5'
import Api from '../../Api'
import { Alert } from 'react-native'

export const emptyImage = {
  id: null,
  url: null,
  main: false,
  loading: false,
  hasImage: false
}

export const alert = (message, title = 'Error', button = 'Okay') => {
  Alert.alert(title, message, [{ text: button }])
}

export const parseServerResponse = (data) => {
  return {
    id: data.id,
    big: data.attributes.image_big_url,
    medium: data.attributes.image_medium_url,
    orig: data.attributes.image_url,
    thumb: data.attributes.image_thumb_url,
    main: data.attributes.main,
    loading: false,
    hasImage: true
  }
}

export const verifyPermissions = async () => {
  const result = await Permissions.askAsync(Permissions.CAMERA_ROLL)
  if (result.status === 'granted') return true
  if (result.status !== 'granted') {
    alert('You need to grant camera roll permission', 'Insufficient permissions!')
    return false
  }
  return true
}

export const uploadImage = async (uri, currentUserId) => {
  if (!currentUserId) alert('Current user id not found')

  const match = /\.(\w+)$/.exec(uri.split('/').pop())
  const type = match ? `image/${match[1]}` : `image`

  // %TODO return when extension is invalid

  const extension = type.split('/')[1]
  const md5 = MD5.hex_md5(Date.now().toString())
  const userId = Math.floor(Math.random() * 10**8) // %TODO get real id

  const name = `${currentUserId}-${userId}-${md5}.${extension}`
  const file = { uri, name, type }

  const form = new FormData()
  form.append('image[file]', file)

  const url = `${Api().apiUrl}/images`

  try {
    const res = await fetch(url, {
      method: 'POST',
      body: form,
      headers: { 
        'Content-Type': 'multipart/form-data',
        ...(await Api().authHeader())
      }
    })
    if (res.status === 201) {
      console.log('Image uploaded')
      return await res.json()
    } else {
      console.log(res.status, await res.json())
      console.log('Error while uploading image')
    }
  }
  catch (err) {
    console.log(err)
    return null
  }
}

export const appendEmptyImages = (images) => {
  const IMAGES_COUNT = 6

  for (let i = images.length; i < IMAGES_COUNT; i += 1) {
    images.push(emptyImage)
  }

  return images
}

export const getImages = async () => {
  const res = await Api().get('/images')
  if (!res.ok) {
    alert('Couldn\'t download images')
    return []
  }

  const images = res.data.data.map(item => parseServerResponse(item))

  return images.slice(0,6)
}

export const appendImage = (response, setCollection, index, localUrl) => {
  setCollection(collection => {
    const newCollection = collection.map(item => ({ ...item }))
    const newItem = { ...parseServerResponse(response.data), thumb: localUrl }
    newCollection[index] = newItem
    return newCollection
  })
}

export const updateAttributeByIndex = (key, value, selectedIndex, setCollection) => {
  setCollection(collection => {
    const selectedItem = { ...collection[selectedIndex] }
    selectedItem[key] = value
    return collection.map((item, index) => {
      if (index === selectedIndex) return selectedItem
      return item
    })
  })
}

export const deleteImage = async (id) => {
  const res = await Api().delete(`/images/${id}`)
  if (res.status === 422) return true // %TODO Remove later
  return res.ok
}

export const setAsMain = async (id) => {
  const res = await Api().put(`/images/${id}/set_as_main`)
  return res.ok
}

export const unMarkMainImage = (setCollection) => {
  setCollection(collection => {
    return collection.map(item => ({ ...item, main: false }))
  })
}

export const findMainImage = (deletedIndex, collection) => {
  const main = collection.filter(item => item.main).length
  console.log('Main', main)
  return collection.findIndex((item, index) => item.url !== null && index !== deletedIndex)
}

export const placeInOrder = (setCollection) => {
  setCollection(collection => {
    const withImages = collection.filter(item => item.thumb)
    let withImagesCopy = withImages.map(item => ({ ...item }))
    return appendEmptyImages(withImagesCopy)
  })
}
