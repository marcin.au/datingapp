import { useEffect } from 'react'
import AsyncStorage from '@react-native-community/async-storage'
import * as Permissions from 'expo-permissions'
import * as ExpoLocation from 'expo-location'
import { timezone } from 'expo-localization'
import {useDispatch, useSelector} from 'react-redux'
import {IS_LOCATION} from '../store/actions/auth'

const Location = (props) => {

  const dispatch = useDispatch()

  useEffect(() => {
    const getLocation = async () => {
      const { status } = await Permissions.askAsync(Permissions.LOCATION)
      if (status !== 'granted') {
        console.log('PERMISSION NOT GRANTED')
        dispatch({ type: IS_LOCATION, isLocation : false})
        return
      }
      const location = await ExpoLocation.getLastKnownPositionAsync()
      const { latitude, longitude } = location.coords
      const locationString = `${latitude},${longitude}`
      AsyncStorage.setItem('location', locationString)
      console.log('LOCATION', locationString)
      dispatch({ type: IS_LOCATION, isLocation : true})
    }
    getLocation()
  }, [])

  useEffect(() => {
    const getTimezone = async () => {
      if (!timezone) return
      AsyncStorage.setItem('timezone', timezone)
      console.log('TIMEZONE', timezone)
    }
    getTimezone()
  }, [])


  return null
}

export default Location
