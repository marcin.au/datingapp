import React, {useState} from 'react'
import {View, ScrollView, Image, StyleSheet, Dimensions, TouchableOpacity, Platform} from 'react-native'
import colors from '../assets/colors'
import Text from './Text'
import { useSelector } from 'react-redux'

const pronoun = (gender) => {
    if (gender === 'male') return 'him'
    if (gender === 'female') return 'her'
    return 'that person'
}

const EndCallView = props => {
    const gender = useSelector(state => state.video.matchedUserGender)

    const choseOption = (
        <View style={styles.optionList}>
            <TouchableOpacity style={styles.optionsFirst} onPress={props.option}>
                <Text style={styles.optionsText}>Stay Matched to Videchat Later</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.options} onPress={props.option2}>
                <Text style={styles.optionsText}>Share Contact Info</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.options} onPress={props.option3}>
                <Text style={styles.optionsText}>Report</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.options} onPress={props.option4}>
                <Text style={styles.optionsText}>Unmatch</Text>
            </TouchableOpacity>
        </View>
    )

    const closeModal = (
        <View style={styles.closeModal}>
            <TouchableOpacity onPress={props.onPress}>
                <Text style={styles.closeModalText}>{props.nameOfCloseButton}</Text>
            </TouchableOpacity>
        </View>
    )

    const theyEndCall = (
            <View style={styles.content}>
                <Text style={styles.text}>{props.partnerName} ended the call</Text>
                <Text style={styles.text}>Good! We didn't like {pronoun(gender)} for you anyway!</Text>
                <Text style={styles.text}>Let's find you a better match.</Text>
            </View>
        )

    const youEndCall = (
            <View style={styles.content}>
                <Text style={styles.text}>You ended the call with {props.partnerName}.</Text>
                <Text style={styles.text}>What would you like to do?</Text>
                {choseOption}
            </View>
    )
    
    const timesUp = (
        <View style={styles.content}>
            <Text style={styles.text}>Time's up on this call, what do you want to do?</Text>
            {choseOption}
        </View>
    )

    const stayMatch = (
        <View style={styles.content}>
            <Text style={styles.text}>Ok great, we will keep you and {props.partnerName} matched.</Text>
            <Text style={styles.text}>Just make sure to remember you've already had a first date when you chat with {props.partnerName} next :)</Text>
            <Text style={styles.text}>We don't want {pronoun(gender)} knowing how popular you are.</Text>
        </View>
    )

    const shareContactInfo = (
        <View style={styles.content}>
            <Text style={styles.text}>
                You and {props.partnerName} have exchanged numbers...
                does someone have a crush?
            </Text>
            <View style={styles.phonenumberBox}>
                <Image source={require('../assets/kartka.png')} style={styles.phoneNumberCard}/>
                <View style={styles.phoneNumber}>
                    <Text style={styles.textPhone}>{props.partnerName}</Text>
                    <Text style={styles.textPhone}>{props.phonePartner}</Text>
                </View>
            </View>
            <TouchableOpacity style={styles.saveContact}>
                <Text style={styles.text}>Save Contact info?</Text>
            </TouchableOpacity>
        </View>
    )

    const report = (
        <View style={styles.content}>
            <Text style={styles.text}>What would you like to report?</Text>
            <View style={styles.optionList}>
                <TouchableOpacity style={styles.optionsFirst} onPress={props.reportOption1}>
                    <Text style={styles.optionsText}>Inappropriate Behavior</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.options} onPress={props.reportOption2}>
                    <Text style={styles.optionsText}>General Meanness</Text>
                </TouchableOpacity>
            </View>
        </View>
    )

    const endCall = (
        <View style={styles.content}>
            <Text style={styles.text}>You have shared your number with {props.partnerName}!</Text>
            <Text style={styles.text}>Fingers crossed {props.partnerName} shares back... might mean {props.partnerName} likes you!</Text>
        </View>
    )

    const getContent = () => {
        if(props.checkEndCall === 1) return theyEndCall
        if(props.checkEndCall === 2) return youEndCall
        if(props.checkEndCall === 3) return timesUp
        if(props.checkEndCall === 4) return stayMatch
        if(props.checkEndCall === 5) return shareContactInfo
        if(props.checkEndCall === 6) return report
        if(props.checkEndCall === 7) return endCall
    }

    return (
            <View style={styles.screen}>
                {getContent()}
                {closeModal}
            </View>

    );
}

const styles = StyleSheet.create({
    screen: {
        height: "100%",
        width: '100%',
        backgroundColor: colors.lightBlue,
        justifyContent: 'center',
        alignItems: "center"
    },
    content: {
        width: "60%",
        justifyContent: "center",
        alignItems: "center"
    },
    text: {
        fontSize: 20,
        textAlign: 'center',
        marginVertical: 15,
    },
    optionList: {
        marginVertical: 20,
        width: "90%"
    },
    options: {
        borderBottomWidth: 1,
    },
    optionsFirst : {
        borderTopWidth: 1,
        borderBottomWidth: 1

    },
    optionsText: {
        fontSize: 22,
        fontWeight: "800",
        marginVertical: 15,
        textAlign: 'center',
    },
    phonenumberBox: {
        width: Dimensions.get("window").width,
        height: 300,
        marginVertical: 30,
        justifyContent: "center",
        alignItems: "center"
    },
    phoneNumberCard: {
        width: "100%",
        height: "100%",
        marginVertical: 30
    },
    phoneNumber: {
        position: 'absolute',
        bottom: 0,
        top: 80,
        left: 0,
        right: 0,
    },
    textPhone: {
        fontFamily: Platform.OS == "ios" ? "Bradley Hand" : "Brown-Bold",
        fontSize: 40,
        textAlign: "center",
        marginVertical: 10
    },
    saveContact: {
        marginTop: 40
    },
    closeModal: {
        position: "absolute",
        justifyContent: 'flex-end', 
        alignItems: 'flex-start',
        bottom: 30,
        left: 20
    },
    closeModalText: {
        fontSize: 20
    }
})

export default EndCallView