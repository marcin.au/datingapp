import { Dimensions } from 'react-native'

export const matchImageSizeToScreen = () => {
  const { width } = Dimensions.get('screen')
  if ( width <= 800 ) return 'medium'
  if (width <= 1125) return 'big' /* iPhone Xs has 2436x1125 px of screen resolution */
  return 'orig'
}
