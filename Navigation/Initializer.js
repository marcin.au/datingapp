import React from 'react'
import AsyncStorage from '@react-native-community/async-storage'
import { useDispatch } from 'react-redux'
import { SET_READY_MODE } from '../store/actions/video'
import { UPDATE_CURRENT_USER, LOGIN } from '../store/actions/auth'
import Api from '../Api'

const Initializer = () => {
  const dispatch = useDispatch()

  React.useEffect(() => {
    (async () => {
      const token = await AsyncStorage.getItem('userToken')
      const id = await AsyncStorage.getItem('user_id')
      if (token) dispatch({ type: LOGIN, token })
      if (id) dispatch({ type: UPDATE_CURRENT_USER, payload: { id } })
      if (!token) return

      const res = await Api().get('/users/me')
      if (!res.ok) return

      const readyMode = res.data.data.attributes.ready_mode
      dispatch({ type: SET_READY_MODE, status: readyMode })
    })()
  }, [])

  return null
}

export default Initializer
