import React from 'react'
import { Image, Dimensions, StyleSheet } from 'react-native'
import { createStackNavigator } from 'react-navigation-stack'
import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import colors from '../assets/colors'
import store from '../store/store'
import RegisterScreen from '../Screens/AuthScreens/RegisterScreen'
import MatchesScreen from '../Screens/MatchesScreen'
import UserSettingsScreen from '../Screens/UserSettingsScreen'
import MainScreen from '../Screens/MainScreen'
import PreferencesScreen from '../Screens/PreferenceScreen'
import UserProfileScreen from  '../Screens/UsersProfileScreen'
import LoginScreen from '../Screens/AuthScreens/LoginScreen'
import SettingsMenu from '../Screens/SettingsMenu'
import ReadyModeIcon from '../Components/ReadyModeIcon'
import { setReadyMode } from '../store/actions/video'

const twoNd = require('../assets/icnos/2nd.png')
const heart = require('../assets/icnos/heart.png')
const settings = require('../assets/icnos/settings.png')

const iconStyle = {
    width: 25,
    height: 25,
    resizeMode: 'contain'
}

const logoStyle = {
    width: 37,
    height: 37,
    resizeMode: 'contain'
}

const navigatorMiddleware = ({ navigation, defaultHandler }) => {
    if (navigation.state.routeName === 'Video') {
        const currentReadyMode = store.getState().video.readyMode
        const readyModeData = store.getState().video.readyModeData
        store.dispatch(setReadyMode(!currentReadyMode, readyModeData))
        return
    }
    if (navigation.state.routes.length > 1) {
        if (navigation.state.routeName === 'Matches') {
            navigation.navigate('MatchesList')
        }
        if (navigation.state.routeName === 'UserSettings') {
            navigation.navigate('Menu')
        }
    }
    defaultHandler()
}

const MainScreenNavigation = createStackNavigator({
    Main: MainScreen
},{ headerMode: 'none' })

const SettingsScreenNavigation = createStackNavigator({
    Menu: SettingsMenu,
    Settings: UserSettingsScreen,
    Preferences: PreferencesScreen
},{ headerMode: 'none' })

const MatchesScreenNavigation = createStackNavigator({
    MatchesList: MatchesScreen,
    Profile: UserProfileScreen
})

const AuthScreenNavigation = createStackNavigator({
    Login: LoginScreen,
    Register: RegisterScreen,
    SettingsWizzard: UserSettingsScreen,
    PreferencesWizzard: PreferencesScreen
},{ headerMode: 'none' })

const tabScreenConfig = {
    MainScreen: {
        screen: MainScreenNavigation,
        navigationOptions: {
            tabBarIcon: (tabInfo) => {
                return <Image source={twoNd} style={logoStyle}/>
            },
            tabBarColor: "white"
        }
    },
    Matches: {
        screen: MatchesScreenNavigation,
        navigationOptions: {
            tabBarIcon: (tabInfo) => {
                return <Image source={heart} style={iconStyle}/>
            },
            tabBarColor: "white"
        }
    },
    UserSettings: {
        screen: SettingsScreenNavigation,
        navigationOptions: {
            tabBarIcon: (tabInfo) => {
                return <Image source={settings} style={iconStyle} />
            },
            tabBarColor: "white",
        }
    },
    Video: {
        screen: MainScreenNavigation,
        navigationOptions: {
            tabBarIcon: (tabInfo) => {
                return <ReadyModeIcon />
            },
            tabBarColor: "white",
        }
    },
}

const DatingTabNavigator = createBottomTabNavigator(tabScreenConfig, {
    defaultNavigationOptions: {
        tabBarOnPress: navigatorMiddleware
    },
    tabBarOptions: {
        activeTintColor: 'black',
        activeBackgroundColor: colors.lightBlue,
        inactiveBackgroundColor: colors.lightYellow,
        showLabel: false,
        style: { height: 85, justifyContent: "center", alignItems: "center", backgroundColor: colors.lightYellow},
        safeAreaInset: { bottom: 'never', top: 'never' },
        tabStyle: {
            borderRadius: 200,
            marginHorizontal: Dimensions.get("window").width <= 380? Dimensions.get("window").width/26 : Dimensions.get("window").width/23,
            width: 65,
            height: 65
        }
    }
})

const MainNavigation = createSwitchNavigator({
    Auth: AuthScreenNavigation,
    Dating: DatingTabNavigator
})

export default createAppContainer(MainNavigation)
