import * as actions from '../actions/video'

const resetState = {
  call: {
    video_call_timed_out: false,
    user_calling: false,
    user_ended_call: false,
    user_extended_call: false,
    user_picked_up_call: false,
    user_rejected_call: false,
    shared_contact_info: false
  },
  currentCall: {
    uid: null,
    started: false,
    name: null,
    phone: null,
    id: null,
  },
  shareContact: {
    phone: null,
    name: null
  },
  videoCallUid: null,
}

const resetCallState = {
  video_call_timed_out: false,
  user_calling: false,
  user_ended_call: false,
  user_extended_call: false,
  user_picked_up_call: false,
}

const initialState = {
  readyMode: false,
  readyModeData: {
    wantsToChangeTo: null,
    lastChange: null
  },
  showAlert: false,
  showNotification: false,
  notificationData: {},
  call: {
    video_call_timed_out: false,
    user_calling: false,
    user_ended_call: false,
    user_extended_call: false,
    user_picked_up_call: false,
    user_rejected_call: false,
    shared_contact_info: false
  },
  currentCall: {
    uid: null,
    started: false,
    name: null,
    phone: null,
    id: null,
  },
  shareContact: {
    phone: null,
    name: null
  },
  videoCallUid: null,
  smallNotification: null,
  smallNotificationData: {},
  matchedUserGender: null,
  rejectName: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.SWITCH_READY_MODE:
      return { ...state, readyMode: !state.readyMode, showAlert: !state.showAlert }
    case actions.SET_READY_MODE:
      return { ...state, readyMode: action.status }
    case actions.SET_READY_MODE_DATA:
      return { ...state, readyModeData: { ...state.readyModeData, ...action.payload } }
    case actions.SHOW_NOTIFICATION:
      return { ...state, showNotification: true }
    case actions.HIDE_NOTIFICATION:
      return { ...state, showNotification: false }
    case actions.SET_NOTIFICATION_DATA:
      return { ...state, notificationData: action.payload }
    case actions.UPDATE_CALL_ATTRIBUTE:
      console.log(`[REDUX] Updating ${Object.keys(action.payload)[0]}`)
      return { ...state, call: { ...state.call, ...action.payload } }
    case actions.SET_CURRENT_CALL:
      return { ...state, currentCall: { ...state.currentCall, ...action.payload } }
    case actions.SET_VIDEO_CALL_UID:
      console.log('REDUX setting uid')
      return { ...state, videoCallUid: action.payload }
    case actions.SET_SHARE_CONTACT_DATA: 
      return {...state, shareContact: action.payload}  
    case actions.SET_SMALL_NOTIFICATION:
      return { ...state, smallNotification: action.context }
    case actions.RESET_CALL:
      return { ...state, ...resetState }
    case actions.RESET_STATE_CALL:
      return { ...state, call: { ...state.call, ...resetCallState } }
    case actions.SET_MATCHED_USER_GENDER:
      return { ...state, matchedUserGender: action.gender }
    case actions.SET_REJECT_NAME:
      return { ...state, rejectName: action.name }
    case actions.SET_SMALL_NOTIFICATION_DATA:
      return { ...state, smallNotificationData: action.payload }
    default:
      return state
  }
}