import * as actions from '../actions/calls'

// for development
const schema = {
  matchedPairId: String,
  matchedUserId: Number,
  matchedUserUuid: String,
  matchedUserData: {}
}

const initialState = {
  callStack: [],
  current: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.PUSH:
      return { ...state, callStack: [...state.callStack, action.payload] }
    case actions.POP:
      if (state.callStack.length === 0) return state
      return {
        ...state,
        callStack: state.callStack.slice(0, state.callStack.length - 1),
        current: state.callStack[state.callStack.length - 1]
      }
    case actions.MOVE_TO_TOP:
      if (action.index === -1) return state

      const itemToMove = state.callStack[action.index]
      const filtered = state.callStack.filter((_, index) => index !== action.index)
      return { ...state, callStack: [...filtered, itemToMove] }
    default:
      return state
  }
}

