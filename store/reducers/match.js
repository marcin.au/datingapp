import {GET_MATCHED_LIST, SORT_LIST, UPDATE_READY_MODE, APPEND_TO_MATCHED_LIST, REMOVE_FROM_LIST, UPDATE_CONTACT } from '../actions/match'

const initialState = {
    matchedList: []
}

export default (state = initialState, action) => {
    switch(action.type){
        case GET_MATCHED_LIST:
            return {
                matchedList: action.matchedList
            }
        case SORT_LIST: {
            return{
                matchedList: action.newList
            }
        }
        case APPEND_TO_MATCHED_LIST:
            const userExists = state.matchedList.find(item => item.profileId === action.payload.profileId)
            if (userExists) return state
            return { ...state, matchedList: [...state.matchedList, action.payload] }
        case UPDATE_READY_MODE: {
          const { id, status } = action.payload
          const newMatchedList = state.matchedList.map(item => {
            if (item.profileId !== id) return item
            return { ...item, ready: status }
          })
          return { ...state, matchedList: newMatchedList }
        }
        case REMOVE_FROM_LIST: {
            const newList = state.matchedList.filter(item => item.uuid !== action.uuid)
            return { ...state, matchedList: newList }
        }
        case UPDATE_CONTACT:
            const { id, status } = action
            const newMatchedList = state.matchedList.map(item => {
                if (item.profileId !== id) return item
                return { ...item, shareInfo: status }
            })
            console.log('NEW MATCHES LIST', newMatchedList.map(item => `share ${item.shareInfo}`))
            return { ...state, matchedList: newMatchedList }
        default:
            return state
    }
}