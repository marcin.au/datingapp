import * as actions from '../actions/profiles'

const initialState = {
  idList: [],
  loading: false,
  availableProfiles: true,
  swiped: 0
}

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.SET_ID_LIST:
      return { ...state, idList: action.list }
    case actions.SET_LOADING:
      return { ...state, loading: action.status }
    case actions.SET_AVAILABLE_PROFILES:
      return { ...state, availableProfiles: action.status }
    case actions.INCREMENT_SWIPED:
      return { ...state, swiped: state.swiped + 1 }
    case actions.RESET_SWIPED:
      return { ...state, swiped: 0 }
    default:
      return state
  }
}
