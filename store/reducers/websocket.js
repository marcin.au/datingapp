import * as actions from '../actions/websocket'
import { knownNotifications } from '../../Components/Websockets/Utils'

let initialState = {}
knownNotifications.forEach(item => {
  initialState[item] = { status: false, data: null }
})

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.NEW_MESSAGE:
      return { ...state, [action.name]: { status: true, data: action.data } }
    case actions.RECEIVE_MESSAGE:
      return { ...state, [action.name]: { status: false } }
    default:
      return state
  }
}
