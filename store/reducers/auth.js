import { LOGIN, SIGNUP, UPDATE_CURRENT_USER, LOGOUT, SET_FIRST_RUN, IS_LOCATION } from '../actions/auth'

const initialState = {
  token: null,
  currentUser: {},
  firstRun: false,
  isLocation: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case LOGIN:
      return { ...state, token: action.token }
    case LOGOUT:
      return { ...state, token: null }
    case SIGNUP: 
      return { ...state, token: action.token }
    case UPDATE_CURRENT_USER:
      return { ...state, currentUser: { ...state.currentUser, ...action.payload } }
    case SET_FIRST_RUN:
      return { ...state, firstRun: action.status } 
    case IS_LOCATION: 
      return {...state, isLocation: action.isLocation}
    default:
      return state
  }
}