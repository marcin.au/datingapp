import Api from "../../Api"
import { Alert } from 'react-native'
import env from '../../env'

export const SET_ID_LIST = 'SET_ID_LIST'
export const SET_LOADING = 'SET_LOADING'
export const SET_AVAILABLE_PROFILES = 'SET_AVAILABLE_PROFILES'
export const INCREMENT_SWIPED = 'INCREMENT_SWIPED'
export const RESET_SWIPED = 'RESET_SWIPED'

const handleErrors = (errors) => {
  errors.forEach(error => {
    Alert.alert('Error', error.message, [{ text: 'Okay' }])
  })
}

export const fetchProfiles = () => {
  return async dispatch => {
    dispatch({ type: RESET_SWIPED })

    dispatch({ type: SET_LOADING, status: true })
    const res = await Api().get(`/profiles?items=${env.profilesPerPage}`)
    dispatch({ type: SET_LOADING, status: false })

    if (res.status === 412) return handleErrors(res.data.data.errors)
    if (res.status !== 200) return

    const totalItems = res.data.meta.count
    dispatch({ type: SET_AVAILABLE_PROFILES, status: totalItems > env.profilesPerPage })

    const ids = res.data.data.map(item => parseInt(item.id))
    dispatch({ type: SET_ID_LIST, list: ids })
  }
}
