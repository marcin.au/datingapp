import store from '../store'
import Api from '../../Api'
import { Alert } from 'react-native'
import env from '../../env'

export const SWITCH_READY_MODE = 'SWITCH_READY_MODE'
export const SET_READY_MODE = 'SET_READY_MODE'
export const SHOW_NOTIFICATION = 'SHOW_NOTIFICATION'
export const HIDE_NOTIFICATION = 'HIDE_NOTIFICATION'
export const SET_NOTIFICATION_DATA = 'SET_NOTIFICATION_DATA'
export const UPDATE_CALL_ATTRIBUTE = 'UPDATE_CALL_ATTRIBUTE'
export const SET_CURRENT_CALL = 'SET_CURRENT_CALL'
export const SET_VIDEO_CALL_UID = 'SET_VIDEO_CALL_UID'
export const SET_SHARE_CONTACT_DATA = 'SET_SHARE_CONTACT_DATA'
export const SET_SMALL_NOTIFICATION = 'SET_SMALL_NOTIFICATION'
export const SET_READY_MODE_DATA = 'SET_READY_MODE_DATA' 
export const RESET_CALL = 'RESET_CALL'
export const RESET_STATE_CALL = 'RESET_STATE_CALL'
export const SET_MATCHED_USER_GENDER = 'SET_MATCHED_USER_GENDER'
export const SET_REJECT_NAME = 'SET_REJECT_NAME'
export const SET_SMALL_NOTIFICATION_DATA = 'SET_SMALL_NOTIFICATION_DATA'

export const setReadyMode = (status, readyModeData) => {
  const TIMEOUT = parseInt(env.readyModeTimeout)
  
  return async dispatch => {
    const currentTime = Math.floor(Date.now() / 1000)

    const request = async status => {
      const action = status ? 'enable' : 'disable'
      const res = await Api().put(`/accounts/ready_mode/${action}`)
      if (res.status === 304) return
      if (res.status !== 200) {
        Alert.alert('Error', `Couldn't ${action} ready mode`, [{ text: 'Okay' }])
        return
      }
      dispatch({ type: SET_READY_MODE_DATA, payload: { lastChange: currentTime, wantsToChangeTo: null } })
      dispatch({ type: SET_READY_MODE, status })
    }

    if (!status) {
      request(false)
      return
    }
    else if (readyModeData.lastChange + TIMEOUT > currentTime) {
      dispatch({ type: SET_READY_MODE_DATA, payload: { wantsToChangeTo: status } })
      const timeLeft = readyModeData.lastChange + TIMEOUT - currentTime - 1
      setTimeout(() => {
        request(true)
      }, timeLeft * 1000)
      return
    }
    request(true)
  }
}

export const finishCall = async () => {
  const uid = store.getState().video.videoCallUid
  const res = await Api().post(`/video_calls/${uid}/extend`)
  console.log(`Finishing call with uid ${uid}, got response status ${res.status}`)
}

// export const nextProposition = async () => {
//   const res = await Api().get('/matched_pairs/next_proposition')
//   console.log(`Requesting next call, got response status ${res.status}`)
// }

export const showSharedcontactNotification = () => {
  store.dispatch({ type: SET_SMALL_NOTIFICATION, context: 'shared-number' })
}

export const showCallExtendedByYouNotification = () => {
  store.dispatch({ type: SET_SMALL_NOTIFICATION, context: 'call-extended-by-you'})
}

export const showCallExtendedNotification = () => {
  store.dispatch({ type: SET_SMALL_NOTIFICATION, context: 'call-extended' })
}

export const showRejectedNotification = () => {
  store.dispatch({ type: SET_SMALL_NOTIFICATION, context: 'rejected' })
}