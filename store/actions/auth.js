import AsyncStorage from '@react-native-community/async-storage'
import Api from '../../Api'
import { SET_READY_MODE } from './video'

export const LOGIN = 'LOGIN'
export const LOGOUT = 'LOGOUT'
export const SIGNUP = 'SIGNUP'
export const UPDATE_CURRENT_USER = 'UPDATE_CURRENT_USER'
export const SET_FIRST_RUN = 'SET_FIRST_RUN'
export const IS_LOCATION = 'IS_LOCATION'

export const login = (email, password) => {
  return async (dispatch) => {
    const payload = { user: { email, password } }
    const res = await Api().post('/sessions', payload, { skipHandlers: true })
    if (res.ok) {
      const { id } = res.data.data
      const token = res.headers.map.authorization
      dispatch({ type: LOGIN, token })
      dispatch({ type: UPDATE_CURRENT_USER, payload: { id } })
      dispatch({ type: SET_READY_MODE, status: res.data.data.attributes.ready_mode })
      if (id) AsyncStorage.setItem('user_id', id)
      saveToStorage(token)
    } else {
      throw new Error('Couldn\'t log in')
    }
  }
}

export const updateCurrentUser = payload => {
  return (dispatch) => {
    dispatch({ type: UPDATE_CURRENT_USER, payload })
  }
}

export const signup = (email, password) => {
  return async (dispatch) => {
    const payload = {
      user: {
        email,
        password
      }
    }
    const res = await Api().post('/registrations', payload, { skipHandlers: true })

    if (res.ok) {
      AsyncStorage.setItem('hasAccount', 'no')
      AsyncStorage.setItem('hasPreferences', 'no')
      const { id } = res.data.data
      const token = res.headers.map.authorization
      dispatch({ type: SIGNUP, token })
      dispatch({ type: UPDATE_CURRENT_USER, payload: { id } })
      dispatch({ type: SET_READY_MODE, status: res.data.data.attributes.ready_mode })
      if (id) AsyncStorage.setItem('user_id', id)
      saveToStorage(token)
    } else if (res.data.data.errors[0].detail === 'has already been taken') {
      throw new Error('same-email')
    } else {
      throw new Error('unknown-error')
    }
  }
}


const saveToStorage = (token) => {
  AsyncStorage.setItem('userToken', JSON.stringify({ token }))
}
