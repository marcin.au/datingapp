import Api from '../../Api'

export const GET_MATCHED_LIST = 'GET_MATCHED_LIST'
export const SORT_LIST = 'SORT_LIST'
export const SEND_SORT_LIST = 'SEND_SORT_LIST'
export const UPDATE_READY_MODE = 'UPDATE_READY_MODE'
export const APPEND_TO_MATCHED_LIST = 'APPEND_TO_MATCHED_LIST'
export const REMOVE_FROM_LIST = 'REMOVE_FROM_LIST'
export const UPDATE_CONTACT = 'UPDATE_CONTACT'

export const fetchMatchedList = () => {
    return async (dispatch) => {
        let setMatchedList
        const res = await Api().get('/matched_pairs')
        const profiles = res.data.data.map(item => {
        const special =  item.attributes.matched_user.data
        const included = item.attributes.matched_user
        const account = included.included[0].attributes
        return {
          uuid: item.id,
          profileId: special.id,
          shareInfo: item.attributes.shared_contact_info,
          avatar: special.attributes.main_image_thumb_url,
          name: account.name,
          age: account.age,
          work: account.work,
          lives_in: account.lives_in,
          phone: account.phone,
          ready: special.attributes.ready_mode,
        }
      })
      setMatchedList = profiles
      dispatch({type: GET_MATCHED_LIST, matchedList: setMatchedList})
    }
}

export const sortList = (newList) => {
    return (dispatch) => {
        dispatch({type: SORT_LIST, newList: newList})
    }
}

export const sendSortedList = (id, position)=> {
    const payload = {
        matched_pair: {
          position
        }
    }
    return async (dispatch) => {
      const res = await Api().put(`/matched_pairs/${id}`, payload)
      if(res.ok){
        console.log("ok")
      }else{
        console.log(res)
      }
     dispatch({type: SEND_SORT_LIST}) 
    }

}

