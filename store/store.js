import { createStore, combineReducers, applyMiddleware } from 'redux'
import ReduxThunk from 'redux-thunk'
import authReducer from './reducers/auth'
import { reducer as alertReducer } from '../Components/Alerts/store'
import videoReducer from './reducers/video'
import matchedReducer from './reducers/match'
import websocketReducer from './reducers/websocket'
import callsReducer from './reducers/calls'
import profilesReducer from './reducers/profiles'

const rootReducer = combineReducers({
  alert: alertReducer,
  auth: authReducer,
  video: videoReducer,
  match: matchedReducer,
  websocket: websocketReducer,
  calls: callsReducer,
  profiles: profilesReducer
})

const store = createStore(rootReducer, applyMiddleware(ReduxThunk))

export default store