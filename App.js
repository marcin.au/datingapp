/**
 * @format
 * @flow strict-local
 */

import React, { useEffect } from 'react'
import { Provider, useDispatch} from 'react-redux'
// import * as Font from 'expo-font'
import DatingNavigation from './Navigation/AppNavigation'
// import * as SplashScreen from 'expo-splash-screen'
import { View, StyleSheet } from 'react-native'
import Alerts from './Components/Alerts/Alerts'
import Location from './Components/Location'
import store from './store/store'
import Websocket from './Components/Websockets/Websocket'
import CallNotification from './Components/CallNotification'
import ShareNumberNotification from './Components/ShareNumberNotification'
import SmallNotification from './Components/SmallNotification'
import ContactPremission from './Components/ContactPremission'
import Worker from './Components/Websockets/Worker'
import AsyncStorage from '@react-native-community/async-storage'
import { SET_FIRST_RUN } from './store/actions/auth'
import Initializer from './Navigation/Initializer'

export default function App() {
  // const [dataLoaded, setDataLoaded] = useState(false)

  // Put stuff that need to be loaded before the app start here
  // useEffect(() => {
  //   (async () => {
  //     try {
  //       await SplashScreen.preventAutoHideAsync()
  //       await Font.loadAsync({
  //         'brown-bold': require('./assets/fonts/brown-bold.ttf')
  //       })
  //       setDataLoaded(true)
  //     }
  //     catch (err) {
  //       console.log(err)
  //     }
  //   })()
  // }, [])

  // useEffect(() => {
  //   if (dataLoaded) SplashScreen.hideAsync()
  // }, [dataLoaded])

  // if (!dataLoaded) return null

  useEffect(() => {
    (async () => {
      const firstRun = await AsyncStorage.getItem('firstRun')
      AsyncStorage.setItem('firstRun', 'false')
      if (firstRun === null || firstRun === 'true') {
        store.dispatch({ type: SET_FIRST_RUN, status: true })
      }
    })()
  }, [])

  const wrapperStyle = StyleSheet.create({
    flex: 1
  })

  return (
    <Provider store={store}>
      <View style={wrapperStyle}>
        <DatingNavigation/>
        <Initializer />
        <Alerts />
        <Location />
        <Websocket />
        <CallNotification />
        <ShareNumberNotification/>
        <SmallNotification />
        <Worker />
      </View>
    </Provider>
  );
}
