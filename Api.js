import AsyncStorage from '@react-native-community/async-storage'
import { Alert } from 'react-native'
import env from './env'

const { apiUrl, hostname } = env

const authHeader = async (path) => {
  if (path === '/sessions' || path === '/registrations') return {}
  const token = await AsyncStorage.getItem('userToken')
  if (token) {
    return { Authorization: JSON.parse(token).token }
  } else {
    return {}
  }
}

const locationHeader = async () => {
  const location = await AsyncStorage.getItem('location')
  if (!location) return {}
  return { 'X-Location': location }
}

const timezoneHeader = async () => {
  const timezone = await AsyncStorage.getItem('timezone')
  if (!timezone) return {}
  return { 'X-Time-Zone': timezone }
}

const request = async (path, payload, method, options = { skipHandlers: false }) => {
  const url = apiUrl + path
  const config = {
    method,
    body: JSON.stringify(payload),
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      ...(await authHeader(path)), // TODO run those asynchrously
      ...(await locationHeader()),
      ...(await timezoneHeader())
    }
  }

  try {
    const res = await fetch(url, config)
    console.log(method, res.status, path)
    if (handlers[res.status] && !options.skipHandlers) {
      handlers[res.status]()
    }
    try {
      const json = await res.json()
      return { ...res, data: json }
    }
    catch (err) {
      return { ...res, data: {} }
    }
  }
  catch (err) {
    console.log(err)
    if (!options.skipHandlers) {
      handlers[0]()
    }
    return { status: 0, data: {}, ok: false, headers: {} }
  }
}

const alert = (text) => Alert.alert('Error', text, [{ text: 'Okay' }])

const handlers = {
  0: () => {
    console.log('No connection to server')
    alert('No connection to server')
  },
  401: async (res) => {
    await AsyncStorage.removeItem('userToken')
    if (global.redirectToLogin) global.redirectToLogin()
    alert('Unauthorized')
  },
  403: (res) => {
    console.log('Forbitten')
    alert('Forbitten')
  },
  500: (res) => {
    console.log('Server error')
    alert('Server error')
  }
}

export default () => ({
  get: (path, payload, options) => request(path, payload, 'GET', options),
  post: (path, payload, options) => request(path, payload, 'POST', options),
  patch: (path, payload, options) => request(path, payload, 'PATCH', options),
  put: (path, payload, options) => request(path, payload, 'PUT', options),
  delete: (path, payload, options) => request(path, payload, 'DELETE', options),
  authHeader: () => authHeader(),
  apiUrl,
  hostname
})
