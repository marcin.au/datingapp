import React, { useState, useEffect } from 'react'
import { View, ScrollView, StyleSheet, TextInput, Alert} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import colors from '../assets/colors'
import { Header, useHeaderHeight } from 'react-navigation-stack';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Api from '../Api'
import UserImages from '../Components/Images/Container'

import Text from '../Components/Text'

import NewButton from '../Components/Button'
import Card from '../Components/Card'
import IconWithInput from '../Components/IconWithInput'
import HeightInput from '../Components/HeightInput';
import Toast from "../Components/Toast"
import GenderSelect from '../Components/GenderSelect';
import { useDispatch } from 'react-redux';
import { updateCurrentUser, LOGOUT } from '../store/actions/auth'

const parseNumberFrom = (num) => {
    return num.replace(/-/g, '')
}

const parseNumberTo = (num) => {
    num = num.replace(/,|-|\./g, '')
    const insert = (str, index, value) => {
        return `${str.substring(0, index)}${value}${str.substring(index, str.length)}`
    }
    if (num.length > 3) {
        num = insert(num, 3, '-')
    }
    if (num.length > 7) {
        num = insert(num, 7, '-')
    }
    return num
}

const UserSettingsScreen = props => {
    const context = props.navigation.state.routeName

    const dispatch = useDispatch()

    const [name, setName] = useState('')
    const [location, setLocation] = useState('')
    const [hometown, setHometown] = useState('')
    const [work, setWork] = useState('')
    const [education, setEducation] = useState('')
    const [age, setAge] = useState('')
    const [height, setHeight] = useState('')
    const [about, setAbout] = useState('')
    const [gender, setGender] = useState('male')
    const [clicked, setClicked] = useState(false)
    const [phone, setPhone] = useState('')
    const [elevatorPitchFocus, setElevatorPitchFocus] = useState(false)

    const [aboutHeight, setAboutHeight] = useState(0)
    const [imageCount, setImageCount] = useState(0)

    useEffect(() => {
        const fetchData = async () => {
            const res = await Api().get('/accounts')
            if (res.status === 200) {
                const data = res.data.data.attributes
                setName(data.name)
                setLocation(data.lives_in)
                setHometown(data.hometown)
                setWork(data.work)
                setEducation(data.education)
                setAge(data.age.toString())
                setHeight(data.height)
                setAbout(data.about)
                setGender(data.gender)
                setPhone(data.phone)
            }
        }
        if (context !== 'SettingsWizzard') {
            fetchData()
        }
    }, [])

    const logoutHandler = async () => {
        const result = await AsyncStorage.removeItem('userToken')
        dispatch({ type: LOGOUT })
        props.navigation.navigate('Login')
        
        if (!result) return
    }

    const alert = (text) => {
        Alert.alert('Error', text, [{ text: 'Okay' }])
    }

    const validate = () => {
        const ageNum = parseInt(age)
        if (isNaN(ageNum)) {
            alert('Invalid age')
            return false
        }
        if (ageNum < 18) {
            alert('Minimal age is 18')
            return false
        }
        if (ageNum > 99) {
            alert('Maximal age is 99')
            return false
        }
        if (name === '') {
            alert('Fill in Your name')
            return false
        }
        if (phone === '') {
            alert('Fill in Your phone number')
            return false
        }
        if (imageCount === 0) {
            alert('You need at least one image')
            return false
        }
        return true
    }

    const forward = () => {
        if (context === 'SettingsWizzard') {
            props.navigation.navigate('PreferencesWizzard')
        } else {
            props.navigation.navigate('Main')
        }
    }

    const submitHandler = async () => {
        if (!validate()) return

        const payload = {
            name,
            hometown,
            work,
            education,
            age,
            height,
            about,
            gender,
            lives_in: location,
            phone
        }

        dispatch(updateCurrentUser({ gender }))

        const res = await Api().post('/accounts', { account: payload })
        if (res.ok) {
            AsyncStorage.setItem('hasAccount', 'yes')
            setClicked(true)
            setTimeout(() => {setClicked(false)}, 1000)
            forward()
        } else {
            setClicked(true)
            setTimeout(() => {setClicked(false)}, 1000)
            alert('Invalid data')
        }
    }

    const elevatorPitchPlaceholder = '140 characters that descripe your awsomeness. How would your friends describe you?'

    return (
        <ScrollView style={styles.screen}>
       <KeyboardAwareScrollView style={styles.content}>
                <TextInput
                    placeholder="Enter First Name"
                    onChangeText={setName}
                    style={styles.nameInput}
                    placeholderTextColor={'black'}
                    value={name}
                />
                <Text style={styles.text}>Upload Profile Photos Below:</Text>
                <UserImages
                    onImageCountChange={count => setImageCount(count)}
                    imagesCount={imageCount}
                />
                <Text style={styles.text}>Let people know about you below:</Text>
                <View style={styles.userInformation}>
                    <Card style={{...styles.card,...styles.userInfoCard}}>
                    <View style={styles.userInfoLeft}>
                        <IconWithInput
                            icon="map-marker-alt"
                            placeholder="Location"
                            value={location}
                            onChangeText={setLocation}
                        />
                        <IconWithInput
                            icon="home"
                            placeholder="Hometown"
                            value={hometown}
                            onChangeText={setHometown}
                        />
                        <IconWithInput
                            icon="briefcase"
                            placeholder="Work"
                            value={work}
                            onChangeText={setWork}
                        />
                        <IconWithInput
                            icon="graduation-cap"
                            placeholder="Education"
                            value={education}
                            onChangeText={setEducation}
                        />
                    </View>
                    <View style={styles.userInfoRight}>
                        <View style={styles.age}>
                        <TextInput
                            placeholder="Age"
                            keyboardType={"number-pad"}
                            style={{...styles.input, width: "70%", textAlign: "center"}}
                            placeholderTextColor={"grey"}
                            value={age}
                            onChangeText={setAge}
                        />
                        </View>
                        <View style={styles.height}>
                        <HeightInput
                            onChange={setHeight}
                            value={height}
                        />
                        </View>
                    </View>
                    </Card>
                </View>
                <View style={styles.userDescription}>
                    <Card style={{...styles.card,...styles.descriptionCard, height: Math.max(125, aboutHeight + 25) }}>
                        <Text style={styles.descTitle}>Elevator Pitch:</Text>
                        <TextInput
                            style={{ ...styles.textArea, height: Math.max(100, aboutHeight) }} 
                            placeholder={elevatorPitchFocus ? null : elevatorPitchPlaceholder}
                            placeholderTextColor={"grey"}
                            maxLength={140}
                            numberOfLines={5} 
                            multiline={true}
                            value={about}
                            onChangeText={setAbout}
                            onContentSizeChange={(e) => setAboutHeight(e.nativeEvent.contentSize.height)}
                            onFocus={() => setElevatorPitchFocus(true)}
                            onBlur={() => setElevatorPitchFocus(false)}
                        />
                    </Card>
                </View>
                <View style={styles.userGender}>
                <Card style={{ ...styles.card, ...styles.genderCard }}>
                    <Text style={styles.descTitle}>I am ...</Text>
                    <GenderSelect
                        value={gender}
                        onChange={setGender}
                        variant="greyscale"
                        style={{ width: '80%' }}
                        thirdGender={false}
                    />
                </Card>
                </View>
                <View style={styles.userNumber}>
                <Card style={{ ...styles.card, ...styles.phoneCard }}>
                    <Text style={styles.descTitle}>Your phone number</Text>
                    <TextInput
                        placeholder="Phone number"
                        keyboardType={"number-pad"}
                        style={{...styles.input, paddingVertical: 5}}
                        placeholderTextColor={"grey"}
                        value={phone}
                        onChangeText={setPhone}
                        onFocus={() => setPhone(parseNumberFrom(phone))}
                        onBlur={() => setPhone(parseNumberTo(phone))}
                    />
                </Card>
                </View>
                <View style={styles.buttonBox}>
                    <NewButton
                        style={{...styles.button,...styles.buttonSave}}
                        color="white"
                        onPress={submitHandler}
                        onLongPress={logoutHandler}
                        delayLongPress={4000}
                    >
                        {context === 'SettingsWizzard' ? 'Next' : 'Save'}
                    </NewButton>
                </View>
                <View style={{width: "100%", alignItems: "center"}}>
                {clicked ? <Toast>You save the changes</Toast>:null}
                </View>
                </KeyboardAwareScrollView>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: colors.lightBlue,
    },
    content: {
        flex: 1,
        marginVertical: Platform.OS === "ios" ?   50 : 10,
        //marginHorizontal: 10,
        paddingBottom: 20,
    },
    nameInput: {
        fontSize: 25,
        marginHorizontal: 10,
        marginBottom: Platform.OS=== "android" ?  0 : 12,
        marginTop: Platform.OS=== "android" ? 3: 10,
        fontFamily: "Brown-Bold"
    },
    text: {
        marginHorizontal: 10,
        fontSize: 16,
        fontWeight: "600",
        marginVertical: 10
    },
    imagePickerBox: {
        width: "100%",
        flexDirection: "row",
        justifyContent: 'center'
    },
    userInformation:{
        justifyContent: "center",
        alignItems: 'center'
    },
    card: {
        width: "92%",
        height: 130,    
        padding: 8,
        backgroundColor: "white",
        shadowColor: 'black',
        shadowOpacity: 0.16,
        shadowOffset: {width: 0, height: 1.5},
        shadowRadius: 8,
        elevation: 6, 
        borderRadius: 8,
    },
    userInfoCard:{
        flexDirection: 'row',
    },
    userInfoLeft: {
        flex: 4,
        justifyContent: "center",
	},
	userInfoRight: {
		borderLeftColor: "grey",
		borderLeftWidth: 2,
		flex: 1,
		justifyContent: 'space-around',
		paddingLeft: 8
    },
    age: {
        borderBottomWidth: 1,
        borderBottomColor: "grey",
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
        fontSize: 16
    },
    height: {
        borderTopWidth: 1,
        borderTopColor: "grey",
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        fontSize: 16
    },
    userDescription: {
        marginVertical: 18,
        justifyContent: "center",
        alignItems: "center"
    },
    descriptionCard:{
        alignItems: "center"
    },
	descTitle: {
		fontSize: 16,
		fontWeight: "bold"
	},
    textArea: {
        paddingHorizontal: 20,
        textAlign: "center",
        fontSize: 16,
        fontWeight: '500',
        fontFamily: "Brown-Bold"
    },
    buttonBox:{
        width: "100%",
        justifyContent: "space-evenly",
        height: 100,
    },
    button:{
        width: 150,
        height: 40,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: {width: 0, height: 2},
        shadowRadius: 8,
        elevation: 5, 
    },
    buttonSave: {
        backgroundColor: colors.button
    },
    genderCard: {
        height: 100,
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    phoneCard: {
        marginTop: 20,
        alignItems: 'center',
        height: 80
    },
    userGender: {
        justifyContent: "center",
        alignItems: "center"
    },
    userNumber: {
        justifyContent: "center",
        alignItems: "center"
    },
    input: {
        fontSize: 16,
        fontFamily: "Brown-Bold"
    }
})

export default UserSettingsScreen