import React, { useState, useEffect , useCallback } from 'react'
import { View, ScrollView, StyleSheet, FlatList, ActivityIndicator, RefreshControl } from 'react-native'
import DraggableFlatList from 'react-native-draggable-flatlist'
import {useDispatch, useSelector} from 'react-redux'

import * as matchedActions from '../store/actions/match'
import Text from '../Components/Text'
import MatchItem from '../Components/MatchItem'
import colors from '../assets/colors'
import CallModal from '../Components/CallModal'
import { SET_CURRENT_CALL, SET_NOTIFICATION_DATA, UPDATE_CALL_ATTRIBUTE, showRejectedNotification } from '../store/actions/video'
import { call } from 'react-native-reanimated'

const MatchesScreen = props => {

  const [loading, setLoading] = useState(true)
  const [startCall, setStartCall] = useState(false)
  const [refresh, setRefresh] = useState(false)
  const [chanelName, setChanelName] = useState('')
  const [partnerName, setPartnerName] = useState('')
  const [phonePartner, setPhonePartner] = useState('')
  const [userId, setUserId] = useState('')

  const dispatch = useDispatch();
  const matchList = useSelector(state => state.match.matchedList)
  const currentCall = useSelector(state => state.video.currentCall)
  const callSocket = useSelector(state => state.video.call)
  const notificationData = useSelector(state => state.video.notificationData)

  // currentCall = { started, uid, name, id }

  // useEffect(() => {
  //   if (callSocket.user_ended_call) {
  //     setStartCall(false)
  //   }
  // }, [callSocket.user_ended_call])

  useEffect(() => {
    if (callSocket.reject_invite || callSocket.user_rejected_call) {
      showRejectedNotification()
      setStartCall(false)
      setChanelName('')
    }
    if (callSocket.reject_invite) {
      dispatch({ type: UPDATE_CALL_ATTRIBUTE, payload: { reject_invite: false } })
    }
    if (callSocket.user_rejected_call) {
      dispatch({ type: UPDATE_CALL_ATTRIBUTE, payload: { user_rejected_call: false } })
    }
  }, [callSocket])

  const getMatchesHandler = useCallback(async() => {
    setRefresh(true)
      try {
        await dispatch(matchedActions.fetchMatchedList())
      }catch(err){
        throw new Error(err)
      }
      setRefresh(false)
  }, [dispatch])

  const postNewPosition = useCallback(async(id, position) => {
    try {
      await dispatch(matchedActions.sendSortedList(id, position))
    }catch(err){
      throw new Error(err)
    }
  }, [dispatch])

  const setCallInfo = (uuid, name, phone, userId) => {
    setChanelName(uuid) 
    setPartnerName(name)
    setPhonePartner(phone)
    setUserId(userId)
    setStartCall(true)
  }


  const renderItem = ({ item, index, drag }) => {
    return (<MatchItem
        cam={item.ready}
        startVideoCall={()=> {setCallInfo(item.uuid, item.name, item.phone, item.profileId)}}
        onSelect={() => props.navigation.navigate('Profile', { id: item.profileId, matchId: item.uuid, share: item.shareInfo, name: item.name })}
        avatar={item.avatar}
        name={item.name}
        age={item.age}
        job={item.work}
        location={item.lives_in}
        drag={drag}
    />)
  }

  useEffect(() => {
      getMatchesHandler().then(() => { setLoading(false)})
  }, [dispatch, getMatchesHandler])
  
  
  useEffect(() => {
    if(!currentCall.started) return
    setChanelName(currentCall.uid)
    setStartCall(currentCall.started)
    setPartnerName(currentCall.name)
    setPhonePartner(notificationData.phone)
    setUserId(currentCall.id)
    dispatch({ type: SET_NOTIFICATION_DATA, payload: { ...notificationData, phone: null } })
    dispatch({ type: SET_CURRENT_CALL, payload: { uid: null, started: false, name: null, id: null }})
  },[currentCall.uid, currentCall.started, currentCall.name, currentCall.id, notificationData.phone])

  const loader = (
    <View style={styles.loader}>
      <ActivityIndicator size="large" color="black"/>
    </View>
  )

  if (loading) return loader

  const callModal = (
    <CallModal
      visible={startCall} 
      user_id={userId} 
      partnerName={partnerName}
      phonePartner={phonePartner}
      chanelName={chanelName}
      hideModal={()=> {setStartCall(false); setChanelName('')}}
    />
  )

  const noMatches = (
    <View style={styles.noMatches}>
      <Text style={styles.noMatchesText}>You don’t have any matches yet, time to get swiping!</Text>
    </View>
  )

  if(startCall) return callModal

  if(matchList.length == 0) return noMatches

  return (
    <View style={styles.screen}>
      <DraggableFlatList 
        // refreshControl={<RefreshControl refreshing={refresh} onRefresh={getMatchesHandler} />}
        style={styles.screen} 
        data={matchList} 
        onDragEnd={(data, index) =>{console.log(data.to, data.data[data.to].uuid), dispatch(matchedActions.sortList(data.data)), postNewPosition( data.data[data.to].uuid, data.to+1)}} 
        keyExtractor={(item, index) => `draggable-item-${item.profileId}`} 
        renderItem={renderItem}/>
    </View>
  );
}

MatchesScreen.navigationOptions = {
  headerTitle:() => <Text style={styles.matchTitle}>Match Queue</Text>,
  headerStyle: {
    backgroundColor: colors.lightYellow,
  },
}

const styles = StyleSheet.create({
  screen: {
    flex: 1
  },
  matchTitle: {
    marginTop: 3,
    fontSize: 24,
    textAlign: "center",
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
  },
  loader: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.lightBlue
  },
  noMatches: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  noMatchesText: {
    fontSize: 20,
    color: "black",
    textAlign: "center",
    marginHorizontal: 20
  }
})

export default MatchesScreen