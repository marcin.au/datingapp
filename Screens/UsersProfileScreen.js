import React, { useState, useEffect, useRef } from 'react'
import { View, ScrollView, StyleSheet, ActivityIndicator, Platform, Alert } from 'react-native'
import Text from '../Components/Text'
import colors from '../assets/colors'
import UserImage from '../Components/UserImage'
import Card from '../Components/Card'
import IconWithLabel from '../Components/IconWithLabel'
import Api from '../Api'
import env from '../env'
import * as Permissions from 'expo-permissions'
import { matchImageSizeToScreen } from '../utils'
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import Entypo from 'react-native-vector-icons/Entypo'
import * as Contacts from 'expo-contacts';
import { useDispatch } from 'react-redux'
import { REMOVE_FROM_LIST } from '../store/actions/match'
import EndCallView from '../Components/EndCallView'

const json = data => JSON.stringify(data, null, 2)

const UserProfile = (props) => {

	let id = null
	let share = null
	let matchId = null
	let name = null

	if (props.navigation) {
		id = props.navigation.state.params.id
		share = props.navigation.state.params.share
		matchId = props.navigation.state.params.matchId
		name = props.navigation.state.params.name
	}
	if (props.id) {
		id = props.id
	}

	if (!id) return <View />

	const [account, setAccount] = useState({})
	const [images, setImages] = useState([])
	const [loading, setLoading] = useState(true)
	const [report, setReport] = useState(false)

	const dispatch = useDispatch();

	const verifyPermissions = async () => {
		const result = await Permissions.askAsync(
		  Permissions.CONTACTS
		);
		if (result.status !== 'granted') {
		  Alert.alert(
			'Insufficient permissions!',
			'You need to grant contact permissions to savecontacts.',
			[{ text: 'Okay'}]
		  );
		  return false;
		}
		return true;
	  };


	useEffect(() => {
		(async () => {
			const res = await Api().get(`/profiles/${id}`)
			const account = res.data.included.find(item => item.type === 'account')
			const images = res.data.included.filter(item => item.type === 'image')
			setImages(images)
			setLoading(false)
			if (!account) return
			setAccount(account.attributes)
		})()
	}, [])

	const parseUrl = (url) => {
		if (!url) return
		return url.replace('localhost', env.hostname)
	}

	const sizes = {
		thumb: 'image_thumb_url',
		medium: 'image_medium_url',
		big: 'image_big_url',
		orig: 'image_url'
	}

	const chosenImageSize = matchImageSizeToScreen()

	let profileImage = null
	if (images.length > 0) {
		profileImage = (
			<UserImage src={parseUrl(images[0].attributes[sizes[chosenImageSize]])} style={styles.section}/>
		)
	}

	const otherImages = images.slice(1)

	const imagesJSX = otherImages.map(item => (
		<UserImage key={item.id} src={parseUrl(item.attributes[sizes[chosenImageSize]])} style={styles.section}/>
	))

	const loader = (
		<View style={styles.loader}>
			<ActivityIndicator size="large" color="black"/>
		</View>
	)

	const menu = useRef();

	const unmatchHandler = async () => {
		const res = await Api().delete(`/matched_pairs/${matchId}`)
		if (res.status === 401) return handleErrors(res.data.data.errors)
		if (res.status === 204) return (
			Alert.alert(
			'Unmatch!',
			`You unmatched with ${name}`,
			[{ text: 'Okay', onPress: () => {
				dispatch({ type: REMOVE_FROM_LIST, uuid: matchId })
				hideMenu()
				props.navigation.goBack()
			}}]
			))
	  }



	const saveContact = async () => {
		const hasPermission = await verifyPermissions();
		if (!hasPermission) {
		  return;
		}else{
        const contactIOS = {
            [Contacts.Fields.FirstName]: `Dating App - ${account.name}`,
            [Contacts.Fields.PhoneNumbers]: [{number: account.phone}]
        }
        const contactAndroid = {
            [Contacts.Fields.FirstName]: `Dating App - ${account.name}`,
            [Contacts.Fields.PhoneNumbers]: [{ label : 'mobile', number: `${account.phone}` }]
        }
        Alert.alert(
			'Save Contact!',
			`You have saved contact as Dating App - ${account.name}`,
			[{ text: 'Okay', onPress: async() => {
                if(Platform.OS === "ios"){
                    const containerId = await Contacts.getDefaultContainerIdAsync()
                    await Contacts.addContactAsync(contactIOS, containerId)
                }
                else {
                    await Contacts.addContactAsync(contactAndroid)
                }
                hideMenu()
			}}]
			)
		}
	}
	
	const reportHandler = () => {
		setReport(true)
	}

	const reportCloseHandelr = () => {
		setReport(false)
	}

	const sendReportHandler = async (reason , reasonAlert) => {
		const payload = {
			report: {
			  reported_id: id,
			  reason: reason
			}
		  }
		  const res = await Api().post('/reports', payload)
		  if (res.status === 401) return handleErrors(res.data.data.errors)
		if (res.status === 201) return (
			Alert.alert(
			'Report!',
			`You report ${name} for ${reasonAlert}`,
			[{ text: 'Okay', onPress: () => {
				props.navigation.goBack()
			}}]
		  ))
	  }

	const hideMenu = () => menu.current.hide();
  
	const showMenu = () => menu.current.show();

	if (loading) return loader

	if (report) return <EndCallView 
						checkEndCall={6} 
						reportOption1={() => {sendReportHandler("inappropriate_behavior", "inappropriate behavior")}}
						reportOption2={() => {sendReportHandler("general_meanness", "general meanness")}} 
						nameOfCloseButton={"Go Back"} 
						onPress={reportCloseHandelr}/>

  return (
		<ScrollView style={styles.screen} showsVerticalScrollIndicator={false}>
			<View style={styles.content}>
				<View style={styles.topBarOptions}>	
					<View style={styles.userNameContainer}>
						<Text style={styles.userNameText}>{account.name || 'Name'}</Text>
					</View>
					{ matchId ?<Menu ref={menu} style={styles.menu} button={<Entypo style={styles.menu} name="dots-three-vertical" size={18} onPress={showMenu}/>}>
						<MenuItem style={styles.option} onPress={reportHandler}>Report user</MenuItem>
						 <MenuItem style={styles.option}  onPress={unmatchHandler}>Delete Match</MenuItem>
						{ share ? <MenuItem style={styles.option}  onPress={saveContact}>Save Contact Info</MenuItem> : null}
					</Menu>: null }
				</View>
				{profileImage}
				<Card style={styles.section}>
					<View style={styles.userInfo}>
						<View style={styles.userInfoLeft}>
							<IconWithLabel label={account.lives_in || 'Location'} icon="map-marker-alt" />
							<IconWithLabel label={account.hometown || 'Hometown'} icon="home" />
							<IconWithLabel label={account.work || 'Work'} icon="briefcase" />
							<IconWithLabel label={account.education || 'Education'} icon="graduation-cap" />
						</View>
						<View style={styles.userInfoRight}>
							<View style={styles.userInfoRightUpper}>
								<Text style={styles.text}>{account.age || 'Age'}</Text>
							</View>
							<View style={styles.userInfoRightLower}>
								<Text style={styles.text}>{account.height === '0"00\'' || !account.height ? 'Height' : account.height}</Text>
							</View>
						</View>
					</View>
				</Card>
				<Card style={{...styles.section,...styles.descriptionCard}}>
					<Text style={styles.descTitle}>Elevator Pitch:</Text>
					<Text style={styles.text}>{account.about || 'Empty so far'}</Text>
				</Card>
				{imagesJSX}
			</View>
		</ScrollView>
  )
}

UserProfile.navigationOptions = {
    headerShown: false
}

const styles = StyleSheet.create({
	screen: {
		flex: 1,
		backgroundColor: colors.lightBlue,

	},
	loader: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: colors.lightBlue
	},
	content: {
    flex: 1,
    alignItems: 'center',
		height: '100%',
		backgroundColor: colors.lightBlue,
		padding: 10,
		marginHorizontal: 10,
		marginVertical:	10
	},
	topBarOptions:{
		width: "95%",
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
	},
	menu: {
		marginBottom: 10,
		marginTop: Platform.OS === "ios" ?  45 : 15,
	},
	userNameContainer: {
		alignItems: "flex-start",
		width: "100%",
		marginTop: Platform.OS === "ios" ?  45 : 15,
		marginBottom: 10
	},
	userNameText: {
		fontSize: 28,
	},
	section: {
        marginVertical: 8,
        width: '100%',
	},
	userInfo: {
		flexDirection: 'row',
	},
	userInfoLeft: {
		flex: 4,
	},
	userInfoRight: {
		borderLeftColor: colors.lightBlack,
		borderLeftWidth: 2,
		flex: 1,
		justifyContent: 'space-around',
		paddingLeft: 8
	},
	userInfoRightLower: {
	},
	userInfoRightUpper: {
		textAlign: "center",
		borderBottomColor: colors.lightBlack,
		borderBottomWidth: 2,
		paddingBottom: 18
	},
	descriptionCard:{
        alignItems: "center"
    },
	descTitle: {
		fontSize: 16,
		fontWeight: "bold"
	},
	text: {
		textAlign: "center",
		fontSize: 16
	},
	option : {
		fontFamily: "Brown-Bold"
	}
})

export default UserProfile
