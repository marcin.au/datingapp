import React, { useState, useEffect } from 'react'
import { connect, useDispatch } from 'react-redux'
import {
  View,
  StyleSheet,
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
  Dimensions
} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import colors from '../assets/colors'
import Input from '../Components/Input'
import NewButton from '../Components/Button'
import Text from '../Components/Text'
import GenderSelect from '../Components/GenderSelect'
import Api from '../Api'
import RangeSlider from '@ptomasroos/react-native-multi-slider'
import Toast from "../Components/Toast"
import { fetchProfiles } from '../store/actions/profiles'

const PreferencesScreen = props => {

  const prefferableGender = () => {
    const { gender } = props.currentUser
    if (!gender) return 'both'
    return gender === 'male' ? 'female' : 'male'
  }

  const [interest, setInterest] = useState(prefferableGender())
  const [neighborhood, setNeighborhood] = useState('')
  const [ageMin, setAgeMin] = useState(20)
  const [ageMax, setAgeMax] = useState(30)
  const [distance, setDistance] = useState(15)
  const [clicked, setClicked] = useState(false)

  const dispatch = useDispatch()

  const context = props.navigation.state.routeName
  const sliderLength = Dimensions.get('window').width / 2

  useEffect(() => {
    const fetchData = async () => {
      const res = await Api().get('/preferences')
      if (res.status === 200) {
        const data = res.data.data.attributes
        setAgeMin(data.age_min)
        setAgeMax(data.age_max)
        setDistance(data.distance)
        setInterest(data.gender)
        setNeighborhood(data.neighborhood)
      }
    }
    if (context !== 'PreferencesWizzard') {
      fetchData()
    }
  }, [])


  const submitHandler = async () => {
    const payload = {
      gender: interest,
      neighborhood,
      age_min: ageMin,
      age_max: ageMax,
      distance
    }
    const res = await Api().post('/preferences', payload)
    if (res.ok) {
      AsyncStorage.setItem('hasPreferences', 'yes')
      setClicked(true)
      setTimeout(() => {setClicked(false)}, 1000)
      props.navigation.navigate('Main')
      dispatch(fetchProfiles())
    } else {
      Alert.alert('Error', 'Invalid data', [{ text: 'Okay' }]);
    }
  }

  const ageString = `${ageMin} - ${ageMax}`

  const onAgeChangeHandler = (values) => {
    setAgeMin(values[0])
    setAgeMax(values[1])
  }

  return (
    <KeyboardAvoidingView behavior={Platform.OS=="ios" ? "padding" : "height"} style={styles.screen}>
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <View style={styles.content}>
          <View style={styles.inputBox}>
            <View style={styles.section}>
              <Text style={styles.text}>I’m interested in:</Text>
              <GenderSelect
                onChange={setInterest}
                value={interest}
                style={{ width: '100%' }}
                variant="greyscale"
                thirdGender={true}
              />
            </View>
            <View style={styles.section}>
              <Input
                label='My Neighborhood:'
                value={neighborhood}
                onChangeText={setNeighborhood}
                labelStyle={{ paddingBottom: 6 }}
                style={{ width: '80%'}}
              />
            </View>
            <View style={styles.section}>
              <Text style={styles.text}>Age Range: {ageString}</Text>
              <RangeSlider
                onValuesChange={onAgeChangeHandler}
                values={[ageMin, ageMax]}
                min={18}
                max={99}
                step={1}
                selectedStyle={{ backgroundColor: colors.button }}
                markerStyle={{ backgroundColor: colors.button, height: 30, width: 30, borderRadius: 50 }}
                containerStyle={{ height: 30 }}
                sliderLength={sliderLength}
              />
            </View>
            <View style={styles.section}>
              <Text style={styles.text}>Maximum Distance: {distance} mi.</Text>
              <RangeSlider
                onValuesChange={(values) => setDistance(values[0])}
                values={[distance]}
                min={1}
                max={30}
                step={1}
                selectedStyle={{ backgroundColor: colors.button }}
                markerStyle={{ backgroundColor: colors.button, height: 30, width: 30, borderRadius: 50 }}
                containerStyle={{ height: 30 }}
                sliderLength={sliderLength}
              />
            </View>
          </View>
          <View style={styles.buttonBox}>
            <NewButton
              style={{...styles.button,...styles.buttonSave}}
              color="white"
              onPress={submitHandler}
            >
              {context === 'PreferencesWizzard' ? 'Start matching!' : 'Save'}
          </NewButton>
          </View> 
          {clicked ? <Toast>You save the changes</Toast>:null}
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  )
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: colors.lightBlue,
  },
  content: {
    justifyContent:'center',
    alignItems: "center",
    width: "100%",
    height: "100%",
  },
  inputBox: {
    width: '65%',
    paddingVertical: 20
  },
  input: {
    marginVertical: 10,
    width: "100%",
    height: 40,
    backgroundColor: "white",
    color: "black",
    borderRadius: 20,
    padding: 10
  },
  buttonBox:{
    width: "100%",
    justifyContent: "center"
  },
  button:{
    width: 220,
    height: 45,
    shadowColor: 'black',
    shadowOpacity: 0.26,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 8,
    elevation: 5, 
  },
  buttonSave: {
    backgroundColor: colors.button
  },
  text: {
    fontSize: 17,
    textAlign: 'center',
    paddingBottom: 6,
    fontFamily: "Brown-Bold"
  },
  section: {
    paddingVertical: 10,
    alignItems: 'center',
  }
})

const mapStateToProps = state => ({
  currentUser: state.auth.currentUser
})

export default connect(mapStateToProps)(PreferencesScreen)
