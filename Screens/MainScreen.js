import React, { useEffect } from 'react';
import { StyleSheet, View, Dimensions, Platform, ActivityIndicator, Alert }from 'react-native';
import CardStack, { Card } from 'react-native-card-stack-swiper';
import Text from "../Components/Text"
import colors from '../assets/colors'
import Api from '../Api'
import ProfileScreen from './UsersProfileScreen'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import { fetchProfiles, INCREMENT_SWIPED } from '../store/actions/profiles'
import * as Permissions from 'expo-permissions'
import * as ExpoLocation from 'expo-location'
import AsyncStorage from '@react-native-community/async-storage'
import {IS_LOCATION} from '../store/actions/auth'

const MainScreen = () => {
  const profiles = useSelector(state => state.profiles)
  const isLocation = useSelector(state => state.auth.isLocation)
  const dispatch = useDispatch()

  console.log(isLocation)
  useEffect(() => {
    if (profiles.idList.length === 0 && profiles.availableProfiles) {
      dispatch(fetchProfiles())
    }
  }, [profiles.idList])

  useEffect(() => {
    if (profiles.swiped === profiles.idList.length) {
      dispatch(fetchProfiles())
    }
  }, [profiles.swiped])

  const cards = profiles.idList.map(id => (
    <Card style={styles.card} key={id}>
			<View style={styles.like}>
        <Text style={styles.text}>LIKE</Text>
			</View>
      <ProfileScreen id={id} />
      <View style={styles.dislike}>
        <Text style={{...styles.text, ...styles.disliketext}}>DISLIKE</Text>
			</View>
    </Card>
  ))

  const loader = (
    <View style={styles.loader}>
      <ActivityIndicator size="large" color="black" />
    </View>
  )

  const swipeHandler = async (index, action) => {
    const id = profiles.idList[index]
    const path = `/profiles/${id}/${action}`
    const res = await Api().post(path)
    if (res.status === 422) {
      Alert.alert('Error', `Couldn't send ${action}`, [{ text: 'Okay' }])
    }
    if (res.status !== 201) return
    dispatch({ type: INCREMENT_SWIPED })
  }

  const getLocation = async () => {
    const { status } = await Permissions.askAsync(Permissions.LOCATION)
    if (status !== 'granted') {
      console.log('PERMISSION NOT GRANTED')
      dispatch({ type: IS_LOCATION, isLocation : false})
      return
    }
    const location = await ExpoLocation.getLastKnownPositionAsync()
    const { latitude, longitude } = location.coords
    const locationString = `${latitude},${longitude}`
    AsyncStorage.setItem('location', locationString)
    console.log('LOCATION', locationString)
    dispatch({ type: IS_LOCATION, isLocation : true})
  }

  if (profiles.loading) return loader

  const refreshButton = (
    <View style={{width: '130%', alignItems: 'center' }}>
      <Text style={styles.noMoreCards}>No new users in your area, try expanding your maximum distance in your preferences. Gotta cast a wide net :)</Text>
      <TouchableOpacity onPress={() => dispatch(fetchProfiles())} style={styles.refresh}>
        <FontAwesome name="refresh" size={50} color="#333" />
      </TouchableOpacity>
    </View>
  )

  const dontHaveLocalization = (
    <View style={{alignItems: 'center', flex: 1, justifyContent: "center"}}>
      <Text style={styles.falseLocalization}>You have to share your localization to explore profiles near you</Text>
      <TouchableOpacity onPress={() => getLocation()} >
        <FontAwesome name="refresh" size={50} color="#333" />
      </TouchableOpacity>
    </View>
  )

  if (!isLocation) return dontHaveLocalization

  return (
    <View style={{ flex: 1 }}>
      
      <CardStack
        horizontalThreshold={Platform.OS == "android" ? 0 : Dimensions.get("window").width/2 }
        outputRotationRange={Platform.OS == "android" ? ['0deg', '0deg', '0deg'] : ['-15deg', '0deg', '15deg']}
        loop={false}
        style={styles.content}
        verticalSwipe={false}
        renderNoMoreCards={() => refreshButton}
        onSwipedRight={(index) => swipeHandler(index, 'like')}
        onSwipedLeft={(index) => swipeHandler(index, 'dislike')}
      >
        {cards}
      </CardStack>
    </View>
  )
};


const styles = StyleSheet.create({
  content:{
    flex: 1,
    justifyContent: 'center',
    zIndex: 99,
    elevation: 99,
    width: "100%",
    marginLeft: "-12.5%"
  },
  card:{
    width: "250%",
    height: Dimensions.get("window").height-85,
    backgroundColor: colors.lightBlue,
    flexDirection: "row",
  },
  like: {
    justifyContent: 'center',
    alignItems: "center",
    backgroundColor: "#71cc8d",
    height: "100%",
    width: "10%"
  },
  dislike: {
    justifyContent: 'center',
    alignItems: "center",
    backgroundColor: "#ed5143",
    height: "100%",
    width: "10%"
  },
  loader: {
    flex: 1,
    justifyContent: 'center'
  },
  text: {
    transform: [{ rotate: '270deg'}],
    fontSize: 24,
    fontWeight: "bold",
    color: "white",
    width: 120,
    textAlign: "center",
    letterSpacing: 3
  },
  disliketext: {
    transform: [{ rotate: '90deg'}],
  },
  refresh: {
    width: 100,
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 100,
    marginRight: 122,
  },
  noMoreCards: {
    fontSize: 20,
    marginBottom: 20,
    marginLeft: 100,
    marginRight: 122,
    textAlign: "center",
  },
  falseLocalization: {
    fontSize: 20,
    marginBottom: 20,
    textAlign: "center",
  }
});

export default MainScreen