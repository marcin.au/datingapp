import React, { useState, useEffect } from 'react'
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Alert,
    Image,
    TouchableWithoutFeedback,
    Keyboard,
    KeyboardAvoidingView,
    Platform,
    Dimensions,
    ActivityIndicator
} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import Text from '../../Components/Text'
import {useDispatch, useSelector} from 'react-redux'
import colors from '../../assets/colors'
import * as authActons from '../../store/actions/auth'
import NewButton from '../../Components/Button'
import Input from '../../Components/Input'

const twoNd = require('../../assets/icnos/2nd.png')

const LoginScreen = props => {
    const [email, setEmail] = useState('dummy@test.pl')
    const [password, setPassword] = useState('qwerty')
    const [tokenChecked, setTokenChecked] = useState(false)
    const firstRun = useSelector(state => state.auth.firstRun)
    const dispatch = useDispatch()

    useEffect(() => {
        (async () => {
            global.redirectToLogin = () => props.navigation.navigate('Login')
            const token = await AsyncStorage.getItem('userToken')
            const hasAccount = (await AsyncStorage.getItem('hasAccount')) === 'yes'
            const hasPreferences = (await AsyncStorage.getItem('hasPreferences')) === 'yes'
            setTokenChecked(true)
            if (!token) return

            if (hasAccount && hasPreferences) {
                props.navigation.navigate('Main')
            } else if (hasAccount && !hasPreferences) {
                props.navigation.navigate('PreferencesWizzard')
            } else if (!hasAccount) {
                props.navigation.navigate('SettingsWizzard')
            }
        })()
    }, [])

    useEffect(() => {
        if (!firstRun) return
        props.navigation.navigate('Register')
    }, [firstRun])

    const validate = () => {
        const error = (err) => {
            Alert.alert('Error', err, [{ text: 'Okay' }])
        }
        const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        if (email === '') {
            error('Email field is empty')
            return false
        } 
        if (!regex.test(String(email).toLowerCase())) {
            error('Email is invalid')
            return false
        }
        if (password === '') {
            error('Password is empty')
            return false
        }
        if (password.length < 6) {
            error('Password should be 6 characters long')
            return false
        }
        return true
    }

    const login = async () => {
        if (!validate()) return

        try {
            await dispatch(authActons.login(email, password))
            props.navigation.navigate('Dating')
        } catch(err) {
            Alert.alert('Error', 'Email and password are invalid', [{ text: 'Okay' }])
        }
    }

    const loader = (
        <View style={styles.loader}>
            <ActivityIndicator color="black" size="large" />
        </View>
    )

    const content = (
        <TouchableWithoutFeedback onPress={() => {Keyboard.dismiss()}}>
            <KeyboardAvoidingView behavior={Platform.OS=="ios" ? "padding" : "height"} style={styles.screen}>
                <View style={styles.content}>
                    <View style={styles.formBox}>
                        <View style={styles.logo}>
                            <View style={styles.appName}>
                                <Image source={twoNd} style={styles.twoNd} />
                                <Text style={styles.logoText}>Date</Text>
                            </View>
                            <Text style={styles.logoBottomText}>"Never need a 1st Date Again"</Text>
                        </View>
                        <View style={styles.inputBox}>
                            <Input label='Email' value={email} onChangeText={setEmail} email keyboardType='email-address'/>
                            <Input label='Password' value={password} onChangeText={setPassword} password secureTextEntry/>
                        </View>
                        <View style={styles.buttonBox}>
                            <NewButton style={{...styles.button,...styles.buttonLogin}} color="white" onPress={login}>Sign In</NewButton>
                        </View>
                    </View>
                    <View style={styles.registerContainer}>
                        <TouchableOpacity onPress={() => {props.navigation.navigate('Register')}} style={styles.registerBox}>
                            <Text style={styles.register}>Sign Up</Text>
                        </TouchableOpacity> 
                    </View>
                </View>
            </KeyboardAvoidingView>
        </TouchableWithoutFeedback>
    )

    return tokenChecked ? content : loader
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.lightBlue,
    },
    content:{
        width: "100%",
        height: "90%",
        justifyContent: "center",
        alignItems:'center',
        marginTop: 50
    },
    formBox:{
        width: "100%",
        height: Dimensions.get("window").height* 0.74,
        justifyContent: "center",
        alignItems:'center', 
    },

    inputBox: {
        width: "65%",
        marginBottom: 10,
        height: Dimensions.get("window").width* 0.5,

    },
    buttonBox:{
        width: "50%",
    },
    buttonBox:{
        width: "50%",
        marginBottom: 50
    },
    button:{
        width: 150,
        height: 40,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: {width: 0, height: 2},
        shadowRadius: 8,
        elevation: 5, 
        fontFamily: "Brown-Bold"
    },
    buttonLogin: {
        backgroundColor: colors.button
    },
    registerContainer:{
        width: "90%",
        textAlign: "left",
        justifyContent: "flex-end",
        height: Dimensions.get("window").width*0.15,
    },
    registerBox: {
        width: 100,
        textAlign: "left",
        height: 30,
        justifyContent: "center",

    },
    register : {
        fontSize: 17,
        letterSpacing: 2,
        marginLeft: 10,
        fontFamily: "Brown-Bold"
    },
    logo: {
        marginVertical: 40,
        justifyContent: "center",
        alignItems: "center"
    },
    logoText: {
        color: "black",
        fontSize: 40,
        fontWeight: "600",
        fontFamily: "Brown-Bold"
    },
    logoBottomText : {
        color: 'black',
        fontSize: 16,fontFamily: "Brown-Bold"
    },
    appName: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    twoNd: {
        width: 35,
        height: 35,
        marginTop: 6
    },
    loader: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.lightBlue
    }

})

export default LoginScreen
