export default {
  lightYellow: '#faf89c',
  lightBlue: '#e5f6f9',
  lightBlack: '#444',
  lightWhite: '#DDD',
  button: '#71cc8d',
  yellowItems: '#FEF4B6'
}
